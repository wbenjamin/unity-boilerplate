Shader "Transparent/Animated Projector" {
Properties {
	_Color1 ("Main Color", Color) = (1,1,1,1)
	_Tex1 ("Base (RGB) Trans (A)", 2D) = "" { TexGen ObjectLinear }
	_Color2 ("Main Color", Color) = (1,1,1,1)
	_Tex2("Base (RGB) Trans (A)", 2D) = "" { TexGen ObjectLinear }
	_Color3 ("Main Color", Color) = (1,1,1,1)
	_Tex3 ("Base (RGB) Trans (A)", 2D) = "" { TexGen ObjectLinear }
}

SubShader {
	//Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	Tags { "RenderType"="Transparent-1" }
		Pass{
		     ZWrite off
	        Fog { Color (0, 0, 0) }
	        Color [_Color1]
	        ColorMask RGB
	        Blend DstColor One
			Offset -1, -1
	        SetTexture [_Tex1] {
			   combine texture * primary, ONE - texture
	           Matrix [_Projector]
	        }
	         SetTexture [_Tex1] {
	           constantColor (0,0,0,0)
	           combine previous lerp (texture) constant
	           Matrix [_ProjectorClip]
	        }
        }
	}
}
