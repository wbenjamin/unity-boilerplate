Shader "Diffuse/Animated Diffuse Spec" {
Properties {
	_MainTex ("Base (RGB) Trans (A)", 2D) = "black" {}
	_Caustics ("Base (RGB)", 2D) = "black" {}
	_CausticsStrength ("Caustics Strength", Range (0.0, 2.0) ) = 1.0
	_CausticsScale ("Caustics scale", Range (1, 30.0) ) = 20.0
	_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
	_BumpMap ("Normalmap", 2D) = "bump" {}
	causticsOffsetAndScale("internal caustics animation offset and scale", Vector) = (.0, .0, .25, .0)
	causticsAnimationColorChannel("internal caustics animation color channel", Vector) = (1.0, .0, .0, 1.0)
}

	SubShader {
		Tags {"Queue"="Geometry" "RenderType"="Opaque"}
		//Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Opaque"}
		LOD 200

		Cull Off
		//Lighting Off 
		//ZWrite Off 
		CGPROGRAM
		#pragma surface surf BlinnPhong 
		
		sampler2D _Caustics;
		sampler2D _MainTex;
		float _CausticsScale;
		float4 causticsOffsetAndScale;
		float _CausticsStrength;
		float4 causticsAnimationColorChannel;
		sampler2D _BumpMap;
		half _Shininess;
		
		struct Input {
			float2 uv_Caustics;
			float2 uv_MainTex;
			float2 uv_BumpMap;
		};
		
		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c1 = tex2D(_Caustics, frac(IN.uv_Caustics * _CausticsScale)* causticsOffsetAndScale.zz + causticsOffsetAndScale.xy);
			fixed4 c2 = tex2D(_MainTex, IN.uv_MainTex);
			//o.Albedo = c1.rgb;
			o.Albedo =  (causticsAnimationColorChannel.x * c1.r
						+ causticsAnimationColorChannel.y * c1.g
						+ causticsAnimationColorChannel.z * c1.b)*_CausticsStrength + c2.rgb ;
			//o.Emission = c1.rgb * c1.a +c2.rgb * c2.a +c3.rgb * c3.a;
			o.Alpha = c1.a * c2.a;
			o.Specular = _Shininess;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		}
		ENDCG
	}
	Fallback "Mobile/VertexLit"
}
