Shader "Transparent/Animated Diffuse V2" {
Properties {
	_Alpha1("Alpha 1", Color) = (1,1,1,1) 
	_Tex1 ("Base (RGB) Trans (A)", 2D) = "white" {}
	_Alpha2("Alpha 2",  Color) = (1,1,1,1)   
	_Tex2("Base (RGB) Trans (A)", 2D) = "white" { }
	_Alpha3("Alpha 3", Color) = (1,1,1,1)   
	_Tex3 ("Base (RGB) Trans (A)", 2D) ="white" { }
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend One OneMinusSrcAlpha
	//ColorMask RGB
	Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,1) }
	BindChannels {
		Bind "Color", color
		Bind "Vertex", vertex
		Bind "TexCoord", texcoord
	}


	// ---- Dual texture cards
	SubShader {
		Pass {
			SetTexture [_Tex1] {
				constantColor [_Alpha1] 
				combine one - texture * constant 
				// Matrix [_Projector]
			}
			SetTexture [_Tex2] {
				constantColor [_Alpha2] 
				combine texture + constant
				// Matrix [_Projector]
			}
			SetTexture [_Tex3] {
				constantColor [_Alpha3] 
				combine texture + constant
				// Matrix [_Projector]
			}

		}
	}
}
}
