Shader "Projector/Caustic" {
  Properties {
  	 _Color ("Main Color", Color) = (1,1,1,1)   	
     _ShadowTex ("Cookie", 2D) = "" { TexGen ObjectLinear }
     _FalloffTex ("FallOff", 2D) = "" { TexGen ObjectLinear }
     _CausticsAnimationTexture ("Caustics animation", 2D) = "white" {}
     _CausticsStrength ("Caustics strength", Range (0.05, 0.3) ) = 0.1
	_CausticsScale ("Caustics scale", Range (10.0, 2000.0) ) = 500.0
     causticsOffsetAndScale("internal caustics animation offset and scale", Vector) = (.0, .0, .25, .0)
	 causticsAnimationColorChannel("internal caustics animation color channel", Vector) = (1.0, .0, .0, .0)
  }
  

	  Subshader {
	     Pass {
	        ZWrite off
      		Fog { Color (0, 0, 0) }
	        Color [_Color]
	        ColorMask RGB
	        Blend DstColor One
			//Offset -1, -1
	        SetTexture [_ShadowTex] {
			   combine texture * primary, ONE - texture
	           Matrix [_Projector]
	        }
	        SetTexture [_FalloffTex] {
	           constantColor (0,0,0,0)
	           combine previous lerp (texture) constant
	           Matrix [_ProjectorClip]
	        }		      
		}
	}
}
