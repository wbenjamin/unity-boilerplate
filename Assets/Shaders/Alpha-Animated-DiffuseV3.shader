Shader "Transparent/Animated Diffuse V3" {
Properties {
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_Caustics ("Base (RGB)", 2D) = "white" {}
	_CausticsScale ("Caustics scale", Range (1, 20.0) ) = 20.0
	causticsAnimationColorChannel("internal caustics animation color channel", Vector) = (1.0, .0, .0, 1.0)
}

SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Pass { 
			//ZWrite Off 
			//Fog { Color (1, 1, 1) }
			//AlphaTest Greater 0 
			//ColorMask RGB 
			Blend DstColor Zero 
			//Offset -1, -1 
			SetTexture [_Caustic] { 
				combine texture, ONE - texture 
				Matrix [_Projector] 
			}
			
			SetTexture [_MainTex] {
		        constantColor (1,1,1,1)
		        combine previous lerp (texture) constant
		        Matrix [_ProjectorClip]
	    	 }
	
	}

}
}
