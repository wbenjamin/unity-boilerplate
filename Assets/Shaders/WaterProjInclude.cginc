#ifndef WATERPLUS_CG_INCLUDED
#define WATERPLUS_CG_INCLUDED
	//#pragma debug
	
	#ifdef PRO_FEATURES
		#define PRO_REFRACTIONS
		#define WATER_EDGEBLEND_ON
	#else
		#undef PRO_REFRACTIONS
		#undef WATER_EDGEBLEND_ON
	#endif
	
	#ifdef FLOWMAP_ANIMATION_ON
		#define FLOWMAP_ALL_ON
	#endif
	
	#define FLAT_HORIZONTAL_SURFACE

	#include "UnityCG.cginc"
	#include "Lighting.cginc"
	#include "AutoLight.cginc"
	
	sampler2D _MainTex;
	float4 _MainTex_ST;
	
	#ifdef FOAM_ON
	//sampler2D _FoamTex;
	half _EdgeFoamStrength;
	#endif
	
	sampler2D _HeightGlossMap;
	sampler2D _DUDVFoamMap;
	
	sampler2D _WaterMap;
	float4 _WaterMap_ST;
	
	#ifdef REFRACTIONS_ON
		#ifdef PRO_REFRACTIONS
			sampler2D _GrabTexture;
		#else
			#ifdef BAKED_REFRACTIONS
			sampler2D _RefractionMap;
			half4 _RefractionMap_ST;
			#endif
			
			#ifdef USE_SECONDARY_REFRACTION
			sampler2D _SecondaryRefractionTex;
			float4 _SecondaryRefractionTex_ST;
			half _refractionsWetness;
			#endif
		#endif
	
	half _Refractivity;
	#endif
	
	#ifdef FLOWMAP_ANIMATION_ON
		sampler2D _FlowMap;
		half flowMapOffset0, flowMapOffset1, halfCycle;
		//float flowSpeed;
		#ifdef FLOWMAP_TIDE
		//float flowTide;
		#endif
	#endif
	
	
	half _normalStrength;
	#ifdef CALCULATE_NORMALS_ON
	sampler2D _NormalMap;
	#endif
	
	#if defined(WATER_EDGEBLEND_ON) || defined(PRO_REFRACTIONS)
	sampler2D _CameraDepthTexture;
	#endif
	
	samplerCUBE _Cube;
	
	half _Reflectivity;
	half _WaterAttenuation;
	
	fixed3 _DeepWaterTint;
	fixed3 _ShallowWaterTint;
	
	//float _DeepWaterCoefficient;
	
	half _Shininess;
	half _Gloss;
	
	//float _yOffset;
	half _Fresnel0;
	
	//half _EdgeBlendStrength;
	
	#ifdef LIGHT_MODEL_ANISOTROPIC
	//half3 anisoLightPos;
	//half anisoAnimationOffset;
	sampler2D _AnisoMap;
	//half2 anisoDirAnimationOffset;
	#endif
	
	#ifdef CAUSTICS_ON
	half _CausticsStrength;
	half _CausticsScale;
	
	sampler2D _CausticsAnimationTexture;
	half3 causticsOffsetAndScale;
	half4 causticsAnimationColorChannel;
	#endif
	
	struct v2f {
    	float4  pos : SV_POSITION;
    	float2	uv_MainTex : TEXCOORD0;
    	
    	half2	uv_WaterMap : TEXCOORD1;
    	
    	half3	viewDir	: TEXCOORD2;
    	
    	#if defined(PERPIXEL_SPECULARITY_ON) || defined(LIGHTING_ON)
    	half3	lightDir : TEXCOORD3;
    	#else
    	//float3 specColor : COLOR;
    	#endif
    	
    	#ifdef LIGHT_MODEL_ANISOTROPIC
	    	#ifdef BAKED_ANISOTROPY_DIR
	    	//float2 anisoDirUV : TEXCOORD4;
	    	#else
		    	#ifndef PERPIXEL_SPECULARITY_ON
		    	half3 anisoDir : TEXCOORD4;
		    	#endif
	    	#endif
    	#endif
    	
    	#ifndef FLAT_HORIZONTAL_SURFACE
    	half3	normal;		//In world space
    	half3  tangent;		//In world space
		half3  binormal : TEXCOORD5;
    	#endif
    	
    	#ifdef REFRACTIONS_ON
	    	#ifdef PRO_REFRACTIONS
	    	half4 grabPassPos : TEXCOORD6;
	    	#else
		    	#ifdef BAKED_REFRACTIONS
		    	half2 uv_RefrMap : TEXCOORD7;
		    	#endif
	    	float2 uv_SecondaryRefrTex : TEXCOORD6;
	    	#endif
    	#endif
    	
    	#if defined(WATER_EDGEBLEND_ON) || defined(PRO_REFRACTIONS)
    	half4 screenPos : TEXCOORD8;
    	#endif
	};
	
	
	v2f vert (appdata_tan v)
	{
	    v2f o;
	    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	    o.uv_MainTex = TRANSFORM_TEX (v.texcoord, _MainTex);
	    
	    o.uv_WaterMap = TRANSFORM_TEX (v.texcoord, _WaterMap);

		
	    o.viewDir = WorldSpaceViewDir(v.vertex);

		#ifndef FLAT_HORIZONTAL_SURFACE
	    o.tangent = mul( _Object2World, half4(v.tangent.xyz, 0.0) );
	    o.normal = mul( half4(v.normal.xyz, 0.0), _World2Object );
	    o.binormal = cross(o.normal, o.tangent) * v.tangent.w;
	    
	    o.tangent = normalize(o.tangent);
	    o.normal = normalize(o.normal);
	    o.binormal = normalize(o.binormal);
		#endif

	    #if defined(PERPIXEL_SPECULARITY_ON) || defined(LIGHTING_ON)
	    //o.lightDir = normalize( mul(rotation, ObjSpaceViewDir(v.vertex) ) );//
	    o.lightDir = normalize(WorldSpaceLightDir( v.vertex ));
	    #else	//Per-vertex specularity
	    #endif
	    
	    #ifdef LIGHT_MODEL_ANISOTROPIC

	    		#ifndef PERPIXEL_SPECULARITY_ON
			    	#ifndef FLAT_HORIZONTAL_SURFACE
			    	o.anisoDir = normalize( cross(o.lightDir, o.normal) );
			    	#else
			    	o.anisoDir = normalize( cross(o.lightDir, half3(0.0, 1.0, 0.0) ) );
			    	#endif
			    #endif

	  
	    	
    	#endif
	    
	    #if defined(WATER_EDGEBLEND_ON) || defined(PRO_REFRACTIONS)
		o.screenPos = ComputeScreenPos(o.pos);
	    #endif
	    
	    #ifdef REFRACTIONS_ON
	    	#ifdef PRO_REFRACTIONS
			    #if UNITY_UV_STARTS_AT_TOP
					//float scale = -1.0;
				#else
					//float scale = 1.0f;
				#endif
			
		    o.grabPassPos.xy = ( float2( o.pos.x, o.pos.y*scale ) + o.pos.w ) * 0.5;
			o.grabPassPos.zw = o.pos.zw;
			#else
			
				#ifdef USE_SECONDARY_REFRACTION
				o.uv_SecondaryRefrTex = TRANSFORM_TEX (v.texcoord, _SecondaryRefractionTex);
				#endif
			#endif
	    #endif
	    
	    return o;
	}
	
	inline float GetLodLevel(float2 _uv) {
		float2 dx = ddx( _uv * 512.0 );	//Multiply by texture size
		float2 dy = ddy( _uv * 512.0 );
		float d = max( dot( dx, dx ), dot( dy, dy ) ) * 10.0;
		return d;//log2(d);//pow(d, 0.5);
	}
	
	#ifdef CAUSTICS_ON
	inline half CalculateCaustics(float2 uv_WaterMap, half waterAttenuationValue) {
		float4 causticsFrame = tex2D(_CausticsAnimationTexture, frac(uv_WaterMap * _CausticsScale) * causticsOffsetAndScale.zz + causticsOffsetAndScale.xy );
		
		return (causticsAnimationColorChannel.x * causticsFrame.r
				+ causticsAnimationColorChannel.y * causticsFrame.g
				+ causticsAnimationColorChannel.z * causticsFrame.b) * _CausticsStrength;
	}
	#endif
	
	inline half CalculateAttenuation(half sinAlpha, half3 normViewDir, half4 waterMapValue) {

		
			#ifdef BINARY_SEARCH_PARALLAX_ATTEN

			#else
				#ifdef IS_MOBILE_ON
			    #else
			    float heightValue = waterMapValue.r;
			    #endif
			    
			    return heightValue;
			    
		    #endif
	}
	
	inline half3 CalculateNormalInTangentSpace(half2 uv_MainTex, out half2 _displacedUV, half3 normViewDir,
											//half sinAlpha,
											half4 waterMapValue
											#ifdef FLOWMAP_ANIMATION_ON
											,half2 flowmapValue, half flowLerp, half flowSpeed
												#ifdef FLOWMAP_ADD_NOISE_ON
												,half flowmapCycleOffset
												#endif
												#endif
												)
	{
		//return float3(0.0, 0.0, 1.0);
		#ifdef CALCULATE_NORMALS_ON
				

				float2 normalmapUV = uv_MainTex;

			_displacedUV = normalmapUV;
			
			//normalmapUV = uv_MainTex;
			//float2 normalmapUV = uv_MainTex;
			
			#ifdef MULTIPLE_SCALE_WAVES_ON
				#ifdef THREE_WAVE_SCALES_ON
				//float3 pNormal = lerp( tex2D (_HeightGlossDUDVMap, i.uv_MainTex * .37), tex2D (_HeightGlossDUDVMap, i.uv_MainTex * .5), waterMapValue.g);
				//pNormal = lerp( texcol, tex2D (_HeightGlossDUDVMap, i.uv_MainTex), waterMapValue.b);
				
				//float3 pNormal = lerp( UnpackNormal( tex2D(_NormalMap, normalmapUV * .37) ),
				//					UnpackNormal( tex2D(_NormalMap, normalmapUV * .5) ), waterMapValue.g * 5.0);
									
				
				//pNormal = lerp( pNormal,
				//					UnpackNormal( tex2D(_NormalMap, normalmapUV) ), waterMapValue.b);
				#else
				//float3 pNormal = lerp( UnpackNormal( tex2D(_NormalMap, normalmapUV * .37) ),
				//					UnpackNormal( tex2D(_NormalMap, normalmapUV) ), waterMapValue.b * .5);
				#endif
			//pNormal.y *= .1;
			pNormal = normalize( pNormal );
			#else
				#ifdef FLOWMAP_ANIMATION_ON	
					#ifdef FLOWMAP_ADD_NOISE_ON
					flowmapCycleOffset *= .2;

					half3 normalT0 = UnpackNormal( tex2D(_NormalMap, normalmapUV + flowmapValue * (flowmapCycleOffset * .5 + flowMapOffset0) ) );
					half3 normalT1 = UnpackNormal( tex2D(_NormalMap, normalmapUV + flowmapValue * (flowmapCycleOffset * .5 + flowMapOffset1) ) );
					#else
					half3 normalT0 = UnpackNormal( tex2D(_NormalMap, normalmapUV + flowmapValue * flowMapOffset0 ) );
					half3 normalT1 = UnpackNormal( tex2D(_NormalMap, normalmapUV + flowmapValue * flowMapOffset1 ) );
					#endif
					
				half3 pNormal = lerp( normalT0, normalT1, flowLerp );
				
				//Account for speed
				pNormal.z /= max(flowSpeed, .1);	//Account for flow map average velocity
				//pNormal.z = tex2D( _WaterMap, normalmapUV * .1 ).g;
				#else
				half3 pNormal = UnpackNormal( tex2D(_NormalMap, normalmapUV) );
				#endif
			#endif
			
			pNormal.z /= _normalStrength;
			pNormal = normalize(pNormal);	//Very very important to normalize!!!
			
			return pNormal;
		#else
		
			return half3(0.0, 0.0, 1.0);
		#endif
	}
	
	#ifdef FLOWMAP_ANIMATION_ON
	inline fixed4 SampleTextureWithRespectToFlow(sampler2D _tex, float2 _uv, half2 flowmapValue, half flowLerp
													#ifdef FLOWMAP_ADD_NOISE_ON
													, flowmapCycleOffset
													#endif
													) {
		#ifdef FLOWMAP_ADD_NOISE_ON
		fixed4 texT0 = tex2D(_tex, _uv + flowmapValue * (flowmapCycleOffset * .5 + flowMapOffset0) );
		fixed4 texT1 = tex2D(_tex, _uv + flowmapValue * (flowmapCycleOffset * .5 + flowMapOffset1) );
		#else
	  	fixed4 texT0 = tex2D(_tex, _uv + flowmapValue * flowMapOffset0 );
		fixed4 texT1 = tex2D(_tex, _uv + flowmapValue * flowMapOffset1 );
		#endif
		
		return lerp( texT0, texT1, flowLerp );
	}
	#endif
	
	#ifdef REFRACTIONS_ON
		#ifdef PRO_REFRACTIONS
		inline fixed3 CalculateRefraction(half2 uv_WaterMap, half3 normViewDir, half3 pNormal, half4 grabPassPos) {
			half3 bump = pNormal.xxy * half3(1,0,1) * .1;	//* refraction strength
			float4 distortOffset = float4(bump.xz * 10.0, 0, 0);
			float4 grabWithOffset = grabPassPos + distortOffset;
			
			//!!!!!!!!!!!!!!!!
			//If depth < w then discard refraction (refract only objects behind water)
			//!!!!!!!!!!!!!!!!
			return tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(grabWithOffset) ).rgb;
		}
		#else
			inline fixed3 CalculateRefraction(
												#ifdef BAKED_REFRACTIONS
												half2 uv_RefrMap,
												#endif
												float2 uv_WaterMap,
												#ifdef USE_SECONDARY_REFRACTION
												half refrStrength,
												float2 uv_SecondaryRefrTex,
												#endif
												
												#ifdef CAUSTICS_ON
												half waterAttenuationValue,
												#endif
												half3 normViewDir,
												// half sinAlpha,
												float2 _dudvValue)
			{
				//Unpack and scale
				float2 dudvValue = _dudvValue * _Refractivity / 100000.0;
				
				fixed3 refractionColor;
				#ifdef BAKED_REFRACTIONS
					#ifdef USE_SECONDARY_REFRACTION
						fixed3 mainRefr = tex2D(_RefractionMap, uv_RefrMap + dudvValue * _RefractionMap_ST.x).rgb;
						fixed3 secondaryRefr = tex2D(_SecondaryRefractionTex, uv_SecondaryRefrTex + dudvValue * _SecondaryRefractionTex_ST.x).rgb * _refractionsWetness;
						refractionColor = lerp(mainRefr, secondaryRefr, refrStrength);
					#else
						refractionColor = tex2D(_RefractionMap, uv_RefrMap + dudvValue * _RefractionMap_ST.x).rgb;
					#endif
				#else
					#ifdef USE_SECONDARY_REFRACTION
						refractionColor = tex2D(_SecondaryRefractionTex, uv_SecondaryRefrTex + dudvValue * _SecondaryRefractionTex_ST.x).rgb * _refractionsWetness;
					#endif
				#endif
				
				
				
				#ifdef CAUSTICS_ON
					refractionColor += CalculateCaustics(uv_WaterMap + dudvValue, waterAttenuationValue);
				#endif
				
				
				return refractionColor;
				
			}
		#endif
	#endif
	
	inline fixed3 CombineEffectsWithLighting(
							#ifdef REFRACTIONS_ON
								fixed3 refraction, half refrStrength,
							#endif
							fixed3 reflection, half3 pNormal,
							half3 normViewDir, half3 normLightDir,
							half2 uv_MainTex, half waterAttenuationValue
							#ifdef LIGHT_MODEL_ANISOTROPIC
								#ifndef PERPIXEL_SPECULARITY_ON
									#ifdef BAKED_ANISOTROPY_DIR
									,half2 anisoDirUV
									#else
									,half3 anisoDir
									#endif
								#else
									,half3 lightDir
								#endif
							#endif
							)
	{

			#ifndef LIGHT_MODEL_ANISOTROPIC
		    half3 halfView = normalize ( normLightDir + normViewDir );	//No need in anisotropic!!!!!!!!!
		    half nDotfloat = saturate( dot (pNormal, halfView) );
		    #endif
		    
		    
		    #ifndef LIGHT_MODEL_FASTEST
		    half nDotView = dot(pNormal, normViewDir);		//Masking
		    half nDotLight = dot(pNormal, normLightDir);	//Shadows (diffuse)
		    #endif

			#ifdef LIGHT_MODEL_ANISOTROPIC	    
		    #ifndef PERPIXEL_SPECULARITY_ON
			    #ifdef BAKED_ANISOTROPY_DIR
			    half3 anisoDir = tex2D(_AnisoMap, anisoDirUV).rgb * 2.0 - 1.0;
			    #endif
		    #else
		    half3 anisoDir = normalize( cross(pNormal, lightDir) );
		    #endif
		    
		    half lightDotT = dot(normLightDir, anisoDir);
		    half viewDotT = dot(normViewDir, anisoDir);
		    
			    #ifdef BAKED_ANISOTROPIC_LIGHTING
			    //float spec = tex2Dlod(_AnisoMap, float4( ( float2(lightDotT, viewDotT) + 1.0 ) * .5, 0.0, 0.0) ).a;
			    half spec = tex2D(_AnisoMap, ( float2(lightDotT, viewDotT) + 1.0 ) * .5).a;
			    //float spec = tex2Dlod(_AnisoMap, ( float2(lightDotT, viewDotT) + 1.0 ) * .5, 0.0, 0.0).a;
			    #else
			    half spec = sqrt(1.0 - lightDotT * lightDotT) * sqrt(1.0 - viewDotT * viewDotT) - lightDotT * viewDotT;
			    spec = pow(spec, _Shininess * 128.0);
			    #endif
			    
			    spec *= _Gloss;
		    
		    //Masking & self-shadowing
		    spec *= max(.0, nDotView) * max(.0, nDotLight);

		    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		    spec *= max( sign( dot(normViewDir, -normLightDir) ), 0.0 );

		   	#endif

		    
		    half specularComponent = spec * _LightColor0.rgb;
		    
	    #ifdef SIMPLIFIED_FRESNEL
	    half fresnel = .5 - nDotView;
	    fresnel = max(0.0, fresnel);
	    #else
	  	half fresnel = _Fresnel0 + (1.0 - _Fresnel0) * pow( (1.0 - nDotView ), 5.0);
	  	fresnel = max(0.0, fresnel - .1);
	  	#endif
	    specularComponent *= fresnel;
	    
	    specularComponent = specularComponent * specularComponent * 10.0;
	    
	    fixed3 finalColor;
	    //finalColor = lerp(_DeepWaterTint, _ShallowWaterTint, saturate(waterAttenuationValue * 1.0) );
	    
	    finalColor = lerp(_ShallowWaterTint, _DeepWaterTint, waterAttenuationValue );
	    
	    #ifdef REFRACTIONS_ON
	    	#ifdef USE_SECONDARY_REFRACTION
	    	finalColor = lerp(refraction, finalColor, saturate( max(waterAttenuationValue, refrStrength * .5) * .8 ) );
	    	#else
	    	finalColor = lerp(refraction, finalColor, refrStrength);
	    	#endif
	    #endif
    
	    finalColor = lerp(finalColor, reflection, clamp(fresnel, 0.0, _Reflectivity) );
	    #if defined(ADD_DIFFUSE_LIGHT_COMPONENT)
	    fixed3 diffuseComponent = (nDotLight * .2 + .8) * _LightColor0.rgb;
	    
	    return (finalColor * diffuseComponent + specularComponent) * 2.0 + UNITY_LIGHTMODEL_AMBIENT.rgb;
	    #else
	    return (finalColor * _LightColor0.rgb + specularComponent) * 2.0 + UNITY_LIGHTMODEL_AMBIENT.rgb;
	    #endif
	}

	fixed4 frag (v2f i) : COLOR
	{
		fixed4 outColor;
        
		fixed4 waterMapValue = tex2D (_WaterMap, i.uv_WaterMap);
		
		//#ifndef IS_MOBILE
		half3 normViewDir = normalize(i.viewDir);
	    
	    #ifdef FLOWMAP_ANIMATION_ON

	    	half2 flowmapValue = tex2D (_FlowMap, i.uv_WaterMap).rg * 2.0 - 1.0;
	    	half flowSpeed = length(flowmapValue);
	    	
	    	half flowLerp = ( abs( halfCycle - flowMapOffset0 ) / halfCycle );
	    	
	    	#ifdef FLOWMAP_ADD_NOISE_ON
	    	half flowmapCycleOffset = 0.0;// * tex2D( _WaterMap, i.uv_MainTex * .3).g;	//Noise
	    	#endif
	    #endif
	    
	    #ifdef CALCULATE_NORMALS_ON
	 	   	//float3 pNormal = UnpackNormal( tex2D(_NormalMap, normalmapUV) );
	    	half2 displacedUV;
			half3 pNormal = CalculateNormalInTangentSpace(i.uv_MainTex, displacedUV, normViewDir,
														//sinAlpha,
														waterMapValue
														#ifdef FLOWMAP_ANIMATION_ON
														,flowmapValue, flowLerp, flowSpeed
															#ifdef FLOWMAP_ADD_NOISE_ON
															,flowmapCycleOffset
															#endif
														#endif
														);
    	
	    	#ifndef FLAT_HORIZONTAL_SURFACE
	    	//Convert to world space from tangent space
	    	pNormal = (i.tangent * pNormal.x) + (i.binormal * pNormal.y) + (i.normal * pNormal.z);
	    	//pNormal = normalize(pNormal);
	    	#else
	    		pNormal = half3(-pNormal.x, pNormal.z, -pNormal.y);
	    	#endif						
		#else
			#ifndef FLAT_HORIZONTAL_SURFACE
				half3 pNormal = i.normal;
			#else
				half3 pNormal = half3(0.0, 1.0, 0.0);
			#endif
	    #endif
	    
	    //Attenuation calculations
	    #ifdef DEPTHMAP_ON
		//float waterAttenuationValue = saturate( CalculateAttenuation(sinAlpha, normViewDir, waterMapValue ) * _WaterAttenuation);
		half waterAttenuationValue = saturate( waterMapValue.r * _WaterAttenuation );
		//float deepwaterCorrection = min(waterAttenuationValue * _DeepWaterCoefficient, 1.0);
	    #endif
	    
	    //
	    //Sample dudv/foam texture
	    #if defined(REFRACTIONS_ON) || defined(FOAM_ON)
		    #if defined(FLOWMAP_ANIMATION_ON) && defined(FLOWMAP_ALL_ON)
				fixed3 dudvFoamValue = SampleTextureWithRespectToFlow(_DUDVFoamMap, i.uv_MainTex, flowmapValue, flowLerp
																		#ifdef FLOWMAP_ADD_NOISE_ON
																		, flowmapCycleOffset
																		#endif
																		).rgb;
				
				//dudvValue *= max(flowSpeed, 0.2);	//Account for flow speed
				
			#else
				fixed3 dudvFoamValue = tex2D(_DUDVFoamMap, i.uv_MainTex).rgb;
			#endif
		#endif
	     
     	#ifdef REFRACTIONS_ON
	     	float2 dudvValue = dudvFoamValue.rg;
			dudvValue = dudvValue * 2.0 - float2(1.0, 1.0);
		
    		#ifdef PRO_REFRACTIONS
				fixed3 refrColor = CalculateRefraction(i.uv_WaterMap, normViewDir, pNormal, i.grabPassPos);
			#else
				fixed3 refrColor = CalculateRefraction(
														#ifdef BAKED_REFRACTIONS
														i.uv_RefrMap
														#endif
														i.uv_WaterMap,
														#ifdef USE_SECONDARY_REFRACTION
														waterMapValue.a,
														i.uv_SecondaryRefrTex,
														#endif
														#ifdef CAUSTICS_ON
														waterAttenuationValue,
														#endif
														normViewDir,// sinAlpha,
														dudvValue);
				//return fixed4(refrColor.rgb, 1.0);
			#endif
			
		#else
			#ifdef CAUSTICS_ON
				fixed3 refrColor = CalculateCaustics(i.uv_WaterMap, waterAttenuationValue);
			#endif
		#endif
	    
	    #if !defined(CALCULATE_NORMALS_ON) && defined(REFRACTIONS_ON)
	    float2 _dudvValue = dudvValue * _normalStrength / 500.0;
	    
	    pNormal.xz += _dudvValue.xy;
	    #endif
	    
	    //
	    //Reflectivity
	    #if defined(REFLECTIONS_ON)
	    	//Normals illusion
	    
			half3 refl = reflect( -normViewDir, pNormal);
				
			fixed3 reflectCol = texCUBE( _Cube , refl ).rgb;
			
			
		#endif
		
		#ifdef FOAM_ON
			fixed foamValue = dudvFoamValue.b;
			half foamAmount = waterMapValue.g * _EdgeFoamStrength;
		
			#ifdef FLOWMAP_ANIMATION_ON
			//Have foam in the undefined areas
			foamAmount = max(foamAmount, flowSpeed * foamValue * .5);// / _EdgeFoamStrength;
			#endif
		//foamAmount = saturate(1.0 - foamAmount);
		
		//If there's foam then the refractions should be darker
		//refrColor = refrColor * (1.0 - foamAmount * .1);
		#endif
		
		outColor.rgb = CombineEffectsWithLighting(
									#ifdef REFRACTIONS_ON
									refrColor, waterMapValue.a,
									#endif
									reflectCol, pNormal,
									normViewDir, i.lightDir,
									i.uv_MainTex, waterAttenuationValue
									#ifdef FOAM_ON
									,foamAmount,
									foamValue
									#endif
									#ifdef LIGHT_MODEL_ANISOTROPIC
									//float3(tex2Dlod(_HeightGlossDUDVMap, float4(i.uv_MainTex, 0, 0) ).b, 0.0, tex2Dlod(_HeightGlossDUDVMap, float4(i.uv_MainTex, 0, 0)).a),
									#ifdef LIGHT_MODEL_ANISOTROPIC
										#ifndef PERPIXEL_SPECULARITY_ON
											#ifdef BAKED_ANISOTROPY_DIR
											//,i.anisoDirUV
											,i.uv_WaterMap
											#else
												#if !defined(CALCULATE_NORMALS_ON) && defined(REFRACTIONS_ON)
												,i.anisoDir + _dudvValue zz
												#else
												,i.anisoDir
												#endif
											#endif
										#else
											,i.lightDir
										#endif
									#endif
									//i.anisoDir,
									//i.anisoParameters,
									#endif
									//#ifdef FLOWMAP_ANIMATION_ON
									//,flowmapValue, flowLerp, flowmapCycleOffset
									//#endif
									);


		//return half4(outColor.rgb, 1.0);
		//
		//Alpha
		#ifdef WATER_EDGEBLEND_ON
		float depth = UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)));
		depth = LinearEyeDepth(depth);
		//depth = Linear01Depth(depth);					
		//x is alpha
		//y used for foam amount
		float4 fadeParameters = float4(0.15, 0.15, 0.5, 1.0);
		float4 edgeBlendFactors = saturate( fadeParameters * (depth-i.screenPos.w) );		
		//edgeBlendFactors.y = 1.0-edgeBlendFactors.y;
		//depth = edgeBlendFactors.x;
		//texcol = float4( (depth-i.screenPos.w) * 0.05, 0.0, 0.0, 1.0);
		
		outColor.a = edgeBlendFactors.x;


		//texcol = float4(i.screenPos.w * 0.01, 0.0, 0.0, 1.0);
		#else
			#ifdef DEPTHMAP_ON
				//outColor.a = 1.0;
				outColor.a = waterMapValue.b;
		    #else
		    	outColor.a = _WaterAttenuation / 2.0;
		    #endif
		    
		     //texcol.a = 1.0;
		#endif
	    return outColor;
	}
				
				
#endif



