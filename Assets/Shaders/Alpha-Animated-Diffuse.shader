Shader "Transparent/Animated Diffuse" {
Properties {
	_MainTex ("Base (RGB) Trans (A)", 2D) = "black" {}
	_Caustics ("Base (RGB)", 2D) = "black" {}
	_CausticsStrength ("Caustics scale", Range (0.0, 2.0) ) = 1.0
	_CausticsScale ("Caustics scale", Range (1, 20.0) ) = 20.0
	causticsOffsetAndScale("internal caustics animation offset and scale", Vector) = (.0, .0, .25, .0)
	causticsAnimationColorChannel("internal caustics animation color channel", Vector) = (1.0, .0, .0, 1.0)
}

	SubShader {
		//Tags {"RenderType"="Opaque"}
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Opaque"}
		LOD 200

		//Blend One One //additive	    
		//Blend DstColor SrcColor //multiply
		//Blend SrcAlpha OneMinusSrcAlpha //alpha blending
		//Blend One OneMinusSrcAlpha //soft additive
		// Upgrade NOTE: commented out, possibly part of old style per-pixel lighting: Blend AppSrcAdd AppDstAdd */
	    //ColorMask RGB
		Cull Off
		//Lighting Off 
		//ZWrite Off 
		CGPROGRAM
		#pragma surface surf Lambert alpha
		
		sampler2D _Caustics;
		sampler2D _MainTex;
		float _CausticsScale;
		float4 causticsOffsetAndScale;
		float _CausticsStrength;
		float4 causticsAnimationColorChannel;
		
		struct Input {
			float2 uv_Caustics;
			float2 uv_MainTex;
		};
		
		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c1 = tex2D(_Caustics, frac(IN.uv_Caustics * _CausticsScale)* causticsOffsetAndScale.zz + causticsOffsetAndScale.xy);
			fixed4 c2 = tex2D(_MainTex, IN.uv_MainTex);
			//o.Albedo = c1.rgb;
			o.Albedo =  (causticsAnimationColorChannel.x * c1.r
						+ causticsAnimationColorChannel.y * c1.g
						+ causticsAnimationColorChannel.z * c1.b)*_CausticsStrength + c2.rgb ;
			//o.Emission = c1.rgb * c1.a +c2.rgb * c2.a +c3.rgb * c3.a;
			o.Alpha = c1.a * c2.a;
		}
		ENDCG
	}
	Fallback "Mobile/VertexLit"
}
