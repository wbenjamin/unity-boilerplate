using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

//[RequireComponent (typeof (Animation))]
//[RequireComponent (typeof (AudioSource))]
[RequireComponent (typeof (Collider))]
[AddComponentMenu ("SIEN/Default/TouchIntent")]

public class TouchIntent : TouchBaseClass
{
	public string className = "com.sien.megawallpaper.GalaxyUnity";
	public string functionName = "currentActivity";
	public string targetName = "popSettings";
	
	/* DETECT DOUBLE CLICK */

	public bool isWaitingDouble = false;
	
	void Awake ()
	{
	
	}
	
//	void Start ()
//	{
//		
//	}

	void Update ()
	{
		
	}
	
	public override void MouseDoubleEvent(){
		OnMouseDouble();
	}
	
	public override void TriggerEvent ()
	{
		OnMouseDown () ;
	}

	void OnMouseDouble(){
		//isMouseUp = true;
		if(isWaitingDouble)TriggerIntent();
	}
	
	void OnMouseDown(){
		if(!isWaitingDouble)TriggerIntent();
	}
	
	public override void RayCaster(Vector3 pos){
	}
	
	
	void TriggerIntent(){
		//#if !EDITOR
		// instersting stuff here: http://forum.unity3d.com/threads/148377-Can-we-access-Android-s-Java-Class-Object
		AndroidJavaClass jc = new AndroidJavaClass(className);
       	if(jc != null){
			AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>(functionName);
        	if(jo != null)jo.Call(targetName);
		}
		//#endif
	}
	
	public override void ReloadEvent (int cam_)
	{
		//OnMouseDown () ;
	}


}
