﻿using UnityEngine;
using System.Collections;

public class RotationConstraint_bak : MonoBehaviour
{

	public Transform target ;
	public Transform constraint ;
	public float limitAngle ;
	public float damping = 6.0f;
	public float dampingLow = 6.0f;
	public float coefForce = 0.5f;
	private bool smooth;
	
	public bool xActive = true;
	public bool yActive = true;
	public bool zActive = true;
	public bool wActive = true;

	void Start ()
	{
		
	}

	void Update ()
	{
		float xTransform = transform.rotation.x;
		float xConstraint = transform.rotation.x;
		
		float yTransform = transform.rotation.y;
		float yConstraint = transform.rotation.y;
		
		float zTransform = transform.rotation.z;
		float zConstraint = transform.rotation.z;
		
		float wTransform = transform.rotation.w;
		float wConstraint = transform.rotation.w;
		
		if(xActive){
			xTransform = target.rotation.x*coefForce;
			xConstraint = constraint.rotation.x*coefForce;
		}
		
		if(yActive){
			yTransform = -target.rotation.y*coefForce;
			yConstraint = -constraint.rotation.y*coefForce;
		}
		
		if(zActive){
			zTransform = target.rotation.z;
			zConstraint = constraint.rotation.z;
		}
		
		if(wActive){
			wTransform = target.rotation.w;
			wConstraint = constraint.rotation.w;
		}
		
		Quaternion tempTransform = new Quaternion(xTransform,yTransform,zTransform,wTransform);
		Quaternion tempConstraint =  new Quaternion(xConstraint,yConstraint,zConstraint,wConstraint);
		
//		Transform tempTransform = new Transform();
//		tempTransform.rotation = target.rotation;
		
		if (Quaternion.Angle (tempTransform, tempConstraint) <= limitAngle)
		{
			
			
			
			//if (smooth)
				transform.rotation = Quaternion.Slerp (transform.rotation, tempTransform, Time.deltaTime * damping) ;
//			else
//				transform.rotation = tempTransform ;
		}
		else
		{
			smooth = true ;
			transform.rotation = Quaternion.Slerp (transform.rotation, constraint.rotation, Time.deltaTime * dampingLow) ;
		}
		if (Quaternion.Angle (transform.rotation, tempTransform) <= 3.0f)
			smooth = false ;
	}
}
