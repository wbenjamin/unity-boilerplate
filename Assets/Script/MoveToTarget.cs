using UnityEngine;

public class MoveToTarget : MonoBehaviour
{
	
	private		Vector3  	target;
	public		Transform   firstTarget;
	public		Transform	secondTarget;
	public		Transform	thirdTarget;
	public		float		speed = 1;
	private		float		setSpeed;
	
    private const float SMOOTH_TIME = 5f; 

    public bool LockX;
    public bool LockY;
    public bool LockZ;
	public bool RotateOnTouch = false;
    public bool useSmoothing = false;
    private Transform thisTransform;
	public float damping = 6.0f;
    private Vector3 velocity;  
	private int countTarget = 2;
	private int count = 0;
	
	void Awake()
	{
		if(secondTarget == null){
			Debug.LogError("You must choose a second target for "+gameObject.name);
		} 
		if(thirdTarget!= null){
			countTarget  = 3;
		}
		setSpeed = speed;
		thisTransform = transform;
		target = firstTarget.position;
        velocity = new Vector3(0.1f, 0.1f, 0.1f)*speed;
		
		InvokeRepeating("SetTarget", Time.deltaTime, 20f+(Random.value*2));

	}

	
	private void LateUpdate()
    {
        var newPos = Vector3.zero;
        if (useSmoothing)
        {
            newPos.x = Mathf.SmoothDamp(thisTransform.position.x, target.x, ref velocity.x, SMOOTH_TIME);
            newPos.y = Mathf.SmoothDamp(thisTransform.position.y, target.y, ref velocity.y, SMOOTH_TIME);
            newPos.z = Mathf.SmoothDamp(thisTransform.position.z, target.z, ref velocity.z, SMOOTH_TIME);

		
	        if (LockX)
	        {
	            newPos.x = thisTransform.position.x;
	        }
	
	        if (LockY)
	        {
	            newPos.y = thisTransform.position.y;
	        } 
	
	        if (LockZ)
	        {
	            newPos.z = thisTransform.position.z;
	        }
			
			
	        transform.position = Vector3.Slerp(transform.position, newPos, Time.time);
		}
		else{
			transform.Translate(Vector3.forward* speed * Time.deltaTime);
		}
		Quaternion rotation = Quaternion.LookRotation(target - thisTransform.position);
		thisTransform.rotation = Quaternion.Slerp(thisTransform.rotation, rotation, Time.deltaTime * damping);
		
		
		if(setSpeed < speed){
				speed-=Time.deltaTime*0.5f;
		}

    }


	void OnCollisionEnter(Collision col)
	{
		
		if(col.gameObject.tag == "Wall"){
			transform.Rotate(0, 180F, 0);
		}
		if(col.gameObject.tag == "Target" || col == null){	
			
		}
	}
	void SetTarget(){
		if(count==0)
			{
				ChangeTarget(secondTarget.position);
				++count;
			}
			else if(count ==1 )
			{ 
				if(countTarget ==2)
					{
						ChangeTarget(firstTarget.position);
						count=0;
					}
					else if(countTarget ==3)
					{
						++count;
						ChangeTarget(thirdTarget.position);
					}
			}
			else if(count == 2)
			{

				ChangeTarget(firstTarget.position);
				count=0;
			}
	}
	
	void OnMouseDown()
	{
		if (RotateOnTouch){
			transform.Rotate(0, 20F, 0);
		}
		velocity *= 2;
		speed *= 2;	
	}

	void ChangeTarget(Vector3 v)
	{		
		target = v;
	}
}
