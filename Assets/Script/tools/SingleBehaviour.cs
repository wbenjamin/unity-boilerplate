using UnityEngine;
/******************************************************************
 * SingleBehaviour (singleton creation of every script you want by heritance) 
 * ---------------
 * function :  allow you to transform class to singleton via heritance from this script
 * 
 * How to use : make heritage from this script to your target script like following example : 
 * 
 * public class EventManager : SingleBehaviour<EventManager>.
 * 
 * this will let you call EventManager from every where by this call : EventManager.instance.whatYouWant
 * ***************************************************************/
public class SingleBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance_;
    public static T instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType(typeof(T)) as T;
            if (instance_ == null)
            {
                GameObject o = new GameObject("_SingleBehaviour_<" + typeof(T).ToString() + ">");
                instance_ = o.AddComponent<T>();
            }
            return instance_;
        }
    }
}