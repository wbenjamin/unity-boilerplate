using UnityEngine;
using System.Collections;

public class RandomMovement : MonoBehaviour {

	// Use this for initialization
	private		Vector3  	target;
	
    private const float SMOOTH_TIME = 5f;
	
	public float randomFactor = 1f;
	public float randomRate = 1f;
	public bool useSmoothing = true;
    public bool LockX;
    public bool LockY;
    public bool LockZ;
	
    private Vector3 velocity; 
    private Vector3 startPosition; 
	private Vector3 targetPosition;
	private bool randomState =true;
	
	void Awake()
	{
		startPosition = transform.position;

        velocity = new Vector3(0.1f, 0.1f, 0.1f);
		
		InvokeRepeating("RandomTarget", 0f, randomRate);

	}

	
	private void LateUpdate()
    {
         if (useSmoothing)
	        {
		Vector3 newPos = Vector3.zero;

        newPos.x = Mathf.SmoothDamp(transform.position.x, targetPosition.x, ref velocity.x, SMOOTH_TIME);
        newPos.y = Mathf.SmoothDamp(transform.position.y, targetPosition.y, ref velocity.y, SMOOTH_TIME);
        newPos.z = Mathf.SmoothDamp(transform.position.z, targetPosition.z, ref velocity.z, SMOOTH_TIME);

        if (LockX)
        {
            newPos.x = startPosition.x;
        }

        if (LockY)
        {
            newPos.y = startPosition.y;
        } 

        if (LockZ)
        {
            newPos.z = startPosition.z;
        }		
		
        transform.position = Vector3.Slerp(transform.position, newPos, Time.time);
		}else{
		
		transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime*2);
		}
	
    }

	
	void RandomTarget(){	
		//Vector3 newTarget = new Vector3();
		if(randomState){
			randomState = false;
			targetPosition = startPosition + (Random.insideUnitSphere * randomFactor);
		}else{
			randomState = true;
			targetPosition = startPosition;
		}
		if (LockX)
        {
            targetPosition.x = startPosition.x;
        }

        if (LockY)
        {
            targetPosition.y = startPosition.y;
        } 

        if (LockZ)
        {
            targetPosition.z = startPosition.z;
        }
	}
}
