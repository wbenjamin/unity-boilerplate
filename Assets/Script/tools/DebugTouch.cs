using UnityEngine;
using System.Collections;
/******************************************************************
 * DebugTouch 
 * ---------------
 * function : log every object you click on from camera view and then 
 * tell you names object hit by your raycast from camera 
 * 
 * How to use : Just put this script on your scene on a gameobject and 
 * click where you want et that will log you name of collided objects 
 *
 * * ***************************************************************/
public class DebugTouch : MonoBehaviour
{

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log("Name = " + hit.collider.name);
                Debug.Log("Tag = " + hit.collider.tag);
                Debug.Log("Hit Point = " + hit.point);
                Debug.Log("Object position = " + hit.collider.gameObject.transform.position);
                Debug.Log("--------------");
            }
        }
    }
}