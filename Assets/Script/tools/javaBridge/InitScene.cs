using UnityEngine;
using System.Collections;
using JsonFx.Json;

public class InitScene : MonoBehaviour {
	
    /************************************************************************************
     * DEFAULTS VARIABLES DECLARATIONS usefull when we are developping to controle every
     * value and settings from unity editor
     * *********************************************************************************/

	//Camera
    private GameObject mainCamera;
    public bool defaultSkullCameraSwitch;
    [Range(0,4)]
    public int defaultSkyboxIndex; 
    //Big black Blocker
	private GUIStyle styleBigBlack;
	private Texture2D texture_black;
	private Rect rect_bigblack;
    private bool isBigBlack = false;
	private bool isLauncher = true;

    // GameObject container that will be full of by FindGameObjectsWithTag inside Start().
    private GameObject[] teethPool;
    private GameObject[] eyesPool;

    // ANIMATIONS SKULL SETTINGS :
    private GameObject skull;
    public bool defaultSkullRotate;
    public int defaultSkullRotateSpeedValue;
    private GameObject jaw;
    // cross and torch Items Behind skull head

    public GameObject[] crossItems;
    [Range(0,2)]
    public int defaultCrossItemsIndex;

    // fire skull head 
    public bool defaultUseHeadFire;
    public GameObject fire;
    public Color defaultHeadFireColor;


    //Background behind the skull head , 2 choices fire or little skull head
    public Color defaultBackgroundFireColor;
    public Color defaultBackgroundSkullColor;
    
    public GameObject[] backgroundAnimations;
    [Range(0, 2)]
    public int defaultBackgroundAnimationsIndex;

    // wearableItems only one at the same time
    public GameObject[] headItems;
    [Range(0, 4)]
    public int defaultHeadItemsIndex;
    
    // ClockItems only one at the same time
    public GameObject[] clockItems;
    [Range(0, 3)]
    public int defaultClockItemsIndex;

    // head Custome items
    [SerializeField]
    public GameObject[] customHeadItems;
    
    public bool defaultUseHorn;
    public bool defaultUseMechaJaw;
    public bool defaultUseEyePatch;
    public bool defaultUseVampireTeeth;
    public bool defaultUseGoldTooth;

    // jaw items only one at the same time
    public GameObject[] jawItems;
    [Range(0, 3)]
    public int defaultJawItemsIndex;

    // wearableItems only one at the same time
    public GameObject[] eyeschoice;
    [Range(0, 2)]
    public int defaultEyesChoiceIndex;
    public bool defaultEyeTrackingIsActive;
    // head material selection
    [Range(0, 4)]
    public int defaultSkullHeadMaterialsIndex;


    // --> PUT YOUR PREF VARS HERE

    /************************************************************************
     * Private variables use as historic which allow use to know if we have to 
     * change value when load playersPrefs
     ***********************************************************************/
    private int currentHeadItem = -1;
    private int currentSkullMatItem = -1;
    private int currentJawValue = -1;
    private int currentCrossValue = -1;
    private int currentEyesValue = -1;
    private int currentClockValue = -1;
    private int currentBgAnimValue = -1;


	private bool isGyro;
	public bool isTest = false;
	
	private bool lowMemory = false;
	private bool firstMemoryLaunch = true;
	
    public bool isGyroDefault = false;

    //Reset is called when you put this script on a game object
    void Reset()
    {
        // be sure that when we put this script on our INITSCENE CTLR object we happend EventManager too.
        if (gameObject.GetComponent<EventManager>() != null)
            gameObject.AddComponent<EventManager>();
    }

    // THIS FUNCTION IS USE TO DISPLAY EVENT LIKE WE ARE ON MOBILE PHONE
    void OnGUI () {
		
		if(isTest)
		{
	        if (GUI.Button(new Rect(0,0,300,100),"PLAY PLAYER PREF"))
	        {
	            LoadPlayerPref();
	        }
	        if (GUI.Button(new Rect(0, Screen.height / 9+50, 100, 50), "swipe RIGHT"))
	        {
	            EventManager.instance.DispatchEvent("{\"eventName\":\"swipe\",\"eventParam\":{\"Velocity\":9721.7744140625,\"Side\":\"right\"}}");
	            //Debug.Log( JsonFx.Json.JsonWriter.Serialize());
	        }
	        if (GUI.Button(new Rect(0, Screen.height / 9+100, 100, 50), "swipe Left"))
	        {
	            EventManager.instance.DispatchEvent("{\"eventName\":\"swipe\",\"eventParam\":{\"Velocity\":-9721.7744140625,\"Side\":\"left\"}}");
	                //Debug.Log( JsonFx.Json.JsonWriter.Serialize());
	        }
	        if (GUI.Button(new Rect(0, Screen.height / 9 + 150, 100, 50), "swipe down"))
	        {
	                //Debug.Log( JsonFx.Json.JsonWriter.Serialize());
	            EventManager.instance.DispatchEvent("{\"eventName\":\"swipe\",\"eventParam\":{\"Velocity\":9721.7744140625,\"Side\":\"down\"}}");
	        }
	        if (GUI.Button(new Rect(0, Screen.height / 9 + 200, 100, 50), "double touch"))
	        {
	                //Debug.Log( JsonFx.Json.JsonWriter.Serialize());
	            EventManager.instance.DispatchEvent("{\"eventName\":\"touch_double\",\"eventParam\":{\"y\":-0.2889847,\"x\":-9.92659}}}");
	        }
	        if (GUI.Button(new Rect(0, Screen.height / 9 + 250, 100, 50), "touch start"))
	        {
	                //Debug.Log( JsonFx.Json.JsonWriter.Serialize());
	            EventManager.instance.DispatchEvent("{\"eventName\":\"touch_start\",\"eventParam\":{\"y\":1,\"x\":-2}}}");
	        }
		}
		
        if(!isLauncher){
             //BIG BLACK SCREEN WHEN YOU don't have the launcher
            GUI.Box(rect_bigblack,"coucou",styleBigBlack);
        }
    }

    /***********************************************************************************
     * FUNCTION CALL AT SCENE INITIALISATION
     * -------------------------------------
     * - inside this function you should get every gameobject you want to find by tag or whatever
     * ********************************************************************************/
    void Awake () {
		
		// BIG BLACK SCREEN WHEN YOU don't have UR LAUNCHER INSTALL ON YOU DEVICE
		texture_black = new Texture2D(Screen.width,Screen.height);
		 for (int y = 0; y < texture_black.height; ++y)
        {
            for (int x = 0; x < texture_black.width; ++x)
            {
               // float r = Random.value;
                Color color = Color.black;
                texture_black.SetPixel(x, y, color);
            }
        }
        texture_black.Apply();
		
		styleBigBlack = new GUIStyle();
		styleBigBlack.normal.background = texture_black;
		rect_bigblack = new Rect (0,0,Screen.width,Screen.height);
		

        // custom data assignement 
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        teethPool = GameObject.FindGameObjectsWithTag("Teeth");
        eyesPool = GameObject.FindGameObjectsWithTag("Eyes");
        skull = GameObject.FindGameObjectWithTag("Skull");
        jaw = GameObject.FindGameObjectWithTag("Jaw");

		//Loading the shared pref
		LoadPlayerPref();
	}

    /***********************************************************************************
     * FUNCTION CALL AFTER AWAKE OF THIS SCRIPT
     * -------------------------------------
     * - inside this function you should set every setting variables you want from java 
     * device wallpaper settings
     * ********************************************************************************/
	void LoadPlayerPref(){

		Debug.Log("############## LoadPlayerPref ################");
		if(lowMemory && !firstMemoryLaunch){
			Application.LoadLevel("Loader");
        }
        else
        {
			
		    firstMemoryLaunch = false;

            // DON'T TOUCH THIS
            string tempIsLauncher = "true";
            if (PlayerPrefs.HasKey("wallpaper_islauncher")) tempIsLauncher = PlayerPrefs.GetString("wallpaper_islauncher");
            if (tempIsLauncher == "true")
            {
                isLauncher = true;
            }
            else
            {
                isLauncher = false;
            }

            //PLACE THE PREFERENCE YOU WANT TO GET HERE////////////////////////////////////////////////
        
            /* SKULL ROTATION */
		    bool skullRotate = getPrefBool("wallpaper_skull_rotate", defaultSkullRotate);
		    if(skullRotate != skull.GetComponent<RotateAnimation>().enabled)  skull.GetComponent<RotateAnimation>().enabled = skullRotate;
		    int skullRotateValue = getPrefInt("wallpaper_skull_rotate_value", defaultSkullRotateSpeedValue);
		    skullRotateValue += 2;
		    if(skullRotateValue !=  skull.GetComponent<RotateAnimation>().speedRotation) skull.GetComponent<RotateAnimation>().speedRotation = skullRotateValue;
       
       
            /************************************
             * camera switch activation
             * *********************************/
            bool cameraSwitchActiv = mainCamera.GetComponent<CameraAnimation>().cameraSwitchIsActive = getPrefBool("wallpaper_skull_camera_switch", defaultSkullCameraSwitch);
            if(!cameraSwitchActiv){
                skull.GetComponent<RotateAnimation>().swipeIsAllow = true;

            }
            else{
                skull.GetComponent<RotateAnimation>().swipeIsAllow = false;

            }
        
            /**********************************
            * Active Cross or torch behind skull 
            * *******************************/
      
            int selectedItem = getPrefInt("wallpaper_cross_items_index", defaultCrossItemsIndex);
		    if(currentCrossValue != selectedItem)
		    {

	            EnableOnlyOneItem(selectedItem, crossItems);
			    currentCrossValue = selectedItem;
			
		    }

            /**********************************
            * Active Head Fire and set color
            * *******************************/
			
		    fire.particleSystem.startColor = getPrefColor("wallpaper_head_fire_color", defaultHeadFireColor);
            fire.SetActive(getPrefBool("wallpaper_use_head_fire", defaultUseHeadFire));
		
			
		    if(fire.activeSelf)
		    {
			    fire.particleSystem.Play();
		    }

            /**********************************
            * Active Background Fire or Skull and set color
            * *******************************/

            selectedItem = getPrefInt("wallpaper_background_animations_index", defaultBackgroundAnimationsIndex);
        
            backgroundAnimations[2].particleSystem.startColor = getPrefColor("wallpaper_background_fire_color", defaultBackgroundSkullColor);
            backgroundAnimations[1].particleSystem.startColor = getPrefColor("wallpaper_background_fire_color", defaultBackgroundFireColor);
		
		    if(currentBgAnimValue != selectedItem)
		    {
			
        	    EnableOnlyOneItem(selectedItem, backgroundAnimations);
			    currentBgAnimValue = selectedItem;
			
		    }


            /**********************************
           * Active or desactive head wears
           * *******************************/
		
            selectedItem = getPrefInt("wallpaper_head_items_value", defaultHeadItemsIndex);
		    if(selectedItem != currentHeadItem)
		    {
			
	            EnableOnlyOneItem(selectedItem, headItems);
			    currentHeadItem = selectedItem;
			
		    }

            /**********************************
            * Active or desactive head Materials
            * *******************************/
            selectedItem = getPrefInt("wallpaper_skull_head_materials", defaultSkullHeadMaterialsIndex);
		 
		    if(selectedItem != currentSkullMatItem)
		    {
	            skull.GetComponentInChildren<MaterialsChoice>().setMaterial(selectedItem);
	            jaw.GetComponent<MaterialsChoice>().setMaterial(selectedItem); 
	            //crest
	            bool currentStatement = headItems[3].activeSelf; 
	            headItems[3].GetComponent<MaterialsChoice>().setMaterial(selectedItem);
			    currentSkullMatItem  = selectedItem;
			
			    if(selectedItem == 1)
			    {
				    customHeadItems[1].SetActive(true);
			    }
			    else
			    {
				    customHeadItems[1].SetActive(false);
			    }
		    }
       
        
            /**********************************
            * Active or desactive clock
            * *******************************/
            selectedItem = getPrefInt("wallpaper_clock_items_value", defaultClockItemsIndex);
		    if(currentClockValue != selectedItem){
	            EnableOnlyOneItem(selectedItem, clockItems);
			    currentClockValue = selectedItem;
		    }


            /**********************************
            * Active and/or disactive custom head items
            * *******************************/

            customHeadItems[0].SetActive(getPrefBool("wallpaper_use_horn", defaultUseHorn));
            //customHeadItems[1].SetActive(getPrefBool("wallpaper_use_mecha_jaw", defaultUseMechaJaw));
            customHeadItems[2].SetActive(getPrefBool("wallpaper_use_eye_patch", defaultUseEyePatch));
            customHeadItems[3].SetActive(getPrefBool("wallpaper_use_vampire_teeth", defaultUseVampireTeeth));
            customHeadItems[4].SetActive(getPrefBool("wallpaper_use_gold_tooth", defaultUseGoldTooth));
        

            /**********************************
            * Active or desactive jaw items wears
            * *******************************/
            selectedItem = getPrefInt("wallpaper_jaw_item_value", defaultJawItemsIndex);
		    if(selectedItem != currentJawValue){
        	    EnableOnlyOneItem(selectedItem, jawItems);
			    currentJawValue = selectedItem;
		    }

            /**********************************
           * Active or desactive jaw items wears
           * *******************************/
            selectedItem = getPrefInt("wallpaper_eyes_choice_value", defaultEyesChoiceIndex);
            //mainCamera.GetComponent<CameraAnimation>().eyesTrackingIsActive = getPrefBool("wallpaper_eyes_tracking", defaultEyeTrackingIsActive);
		    //mainCamera.GetComponent<CameraAnimation>().restartCamera();
		
		    if(selectedItem != currentEyesValue)
		    {
        	    EnableOnlyOneItem(selectedItem, eyeschoice);
			    currentEyesValue = selectedItem;
		    }
			
            /**********************************
           * Gyro
           * *******************************/
		    isGyro = getPrefBool("wallpaper_gyro",isGyroDefault);
		    activateGyro(isGyro);
		
		    /* change skybox*/
		    mainCamera.GetComponent<MultiSkyBox>().SetSkyBox(getPrefInt("wallpaper_skybox", defaultSkyboxIndex));
			
			
        }

	    //////////////////////////////////////////////////////////////////////////////////////////
	}


    /***********************************************************************************
    * FUNCTIONS USED AS TOOLS FOR GETS DIFFERENTS TYPE OF VALUES FROM SETTINGS
    * ********************************************************************************/
	 
    
    void EnableOnlyOneItem(int value,GameObject[] array) {
       
        foreach (GameObject item in array)
        {
            //Debug.Log(item.ToString());

            if (item != null && item.activeSelf)
            {
                item.SetActive(false);
            }
        }
        // Active selected item
        if (value != array.Length-1 &&  array[value] != null)
        {
            array[value].SetActive(true);
        }
    }

    // pNameParam: name of android pref and param and we will send back if the para m is true or false
    // pDefaultValue: is  true or false by default?
    bool getPrefBool(string pNameParam, bool pDefaultValue) {
        string lTempBool;

        if (PlayerPrefs.HasKey(pNameParam))
        {
            lTempBool = PlayerPrefs.GetString(pNameParam);
            if (lTempBool == "true")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return pDefaultValue;
    }
	
	string getPrefString(string pNameParam, string pDefaultValue) {

        if (PlayerPrefs.HasKey(pNameParam))
        {
            return PlayerPrefs.GetString(pNameParam);
           
        }
		
        return pDefaultValue;
    }
	
	Color getPrefColor(string pNameParam, Color pDefaultValue) {
        Color lTempColor;

        if (PlayerPrefs.HasKey(pNameParam))
        {
			lTempColor = HexToRGB(PlayerPrefs.GetString(pNameParam).Substring(1));
            return lTempColor;
           
        }
		
        return pDefaultValue;
    }
	
	
	int getPrefInt(string pNameParam, int pDefaultValue) {

        if (PlayerPrefs.HasKey(pNameParam))
        {
            return PlayerPrefs.GetInt(pNameParam);
        }
		
        return pDefaultValue;
    }
	
	string getPrefPath(string pNameParam, string pDefaultValue) {

        if (PlayerPrefs.HasKey(pNameParam))
        {
            return "file://"+PlayerPrefs.GetString(pNameParam);
           
        }
		
        return pDefaultValue;
    }

	
	private IEnumerator LoadImages(string pFile,GameObject pObject){
		Debug.Log("LoadImages() --->pFile= "+pFile);
		var www = new WWW(pFile);
		yield return www;
		// www.LoadImageIntoTexture(lObject.renderer.material.GetTexture("_MainTex") as Texture2D);
		if(www.size>0){
			Debug.Log("LoadImages() image is OK!");
		if(pObject!=null) pObject.renderer.material.SetTexture("_MainTex",www.texture);
		
		
		//Application.CaptureScreenshot("Screenshot.png");
		
		/*GameObject lObject2 = GameObject.Find("Main Camera");
		CameraAnimation lScript = (CameraAnimation) lObject2.GetComponent(typeof(CameraAnimation));
		lScript.TriggerScreenCapture(screenCaptureCameraPos,screenCaptureFileName);*/
		}
	}
	
	public void activateGyro(bool val_){
		GameObject lObject2 = GameObject.FindGameObjectWithTag("MainCamera");
		CameraAnimation lScript = (CameraAnimation) lObject2.GetComponent(typeof(CameraAnimation));
		lScript.activateGyro(val_);
	}


	void ApplyTextureOn(string pNameObject,Texture pText){
		GameObject lObject = GameObject.Find(pNameObject);
		if(lObject!=null && lObject.renderer != null) lObject.renderer.material.mainTexture = pText;
	}
	/*
	void ApplyTextureOn(string pNameObject,Texture pText,int pIndex){
		GameObject lObject = GameObject.Find(pNameObject);
		//if(lObject!=null && lObject.renderer != null )lObject.renderer.materials[pIndex].SetTexture("_MainTex",pText);
		if(lObject!=null && lObject.renderer != null )lObject.renderer.material.mainTexture = pText;
	}*/
	
	void ApplyVisibilityOn(string pNameObject,bool pBool){
		GameObject lObject = GameObject.Find(pNameObject);
		if(lObject!=null && lObject.renderer != null)lObject.renderer.enabled = pBool;
	}
	
	void ApplyActiveOn(string pNameObject,bool pBool){
		GameObject lObject = GameObject.Find(pNameObject);
		if(lObject!=null)lObject.active = pBool;
	}
	
	void ApplyVisibilityOnObject(GameObject lObject,bool pBool){
		if(lObject!=null && lObject.renderer != null)lObject.renderer.enabled = pBool;
	}
	
	void ApplyVisibilityGroup(string pNameObject,bool pBool){
		if(GameObject.Find(pNameObject)!=null){
	        foreach (Transform tran in GameObject.Find(pNameObject).transform)
	
	        {
				if(tran.renderer != null) tran.renderer.enabled = pBool;
	        }
		}
	}
	
	void ApplyVisibilityObjectGroup(GameObject pNameObject,bool pBool){
			if(pNameObject.renderer != null)pNameObject.renderer.enabled = pBool;
	        foreach (Transform tran in pNameObject.transform)
	
	        {
				if(tran.renderer != null) tran.renderer.enabled = pBool;
	        }
	}
	
	void ApplyObjectColor(string pNameObject,string pNameColor,Color pColor,int pInt){
		GameObject lObject = GameObject.Find(pNameObject);
		//if(lObject!=null && lObject.renderer != null) lObject.renderer.materials[0].get = pText;
		if(lObject!=null && lObject.renderer != null ) lObject.renderer.materials[pInt].SetColor(pNameColor,pColor);
	}
	
	void ApplyGameObjectColor(GameObject pGameObject,string pNameColor,Color pColor,int pInt){
		//if(lObject!=null && lObject.renderer != null) lObject.renderer.materials[0].get = pText;
		if(pGameObject!=null && pGameObject.renderer != null ) pGameObject.renderer.materials[pInt].SetColor(pNameColor,pColor);
	}
	
	//COLOR CONVERTOR
	public static Color HexToRGB(string color) {
        float red = (HexToInt(color[1]) + HexToInt(color[0]) * 16f) / 255f;
        float green = (HexToInt(color[3]) + HexToInt(color[2]) * 16f) / 255f;
        float blue = (HexToInt(color[5]) + HexToInt(color[4]) * 16f) / 255f;
        Color finalColor = new Color { r = red, g = green, b = blue, a = 1 };
        return finalColor;
    }
	
	 private static int HexToInt(char hexChar) {
        switch (hexChar) {
            case '0': return 0;
            case '1': return 1;
            case '2': return 2;
            case '3': return 3;
            case '4': return 4;
            case '5': return 5;
            case '6': return 6;
            case '7': return 7;
            case '8': return 8;
            case '9': return 9;
            case 'A': return 10;
            case 'B': return 11;
            case 'C': return 12;
            case 'D': return 13;
            case 'E': return 14;
            case 'F': return 15;
        }
        return -1;
    }
	
	private string messageWaitingText(int num){
		// what the langage?
		string currentLang = Application.systemLanguage.ToString();
		//ES
		if(currentLang.ToLower().Contains("es")){
			
			if(num==0){
				return "YUsted tiene ningún mensaje! Disfruta de tu día!";
			}else if(num==1){
				return "Usted ha 1 Texto de mensaje en espera!";
			}else{
				return "Has "+num+" de mensajes de texto en espera!";
			}
			
		//PT
		}else if(currentLang.ToLower().Contains("pt")){
			
			if(num==0){
				return "Você não tem nenhuma mensagem! Aproveite o seu dia!";
			}else if(num==1){
				return "Você tem 1 mensagem de texto à espera!";
			}else{
				return "Você "+num+" mensagens de texto em espera.";
			}
			
		//IT
		}else if(currentLang.ToLower().Contains("it")){
			
			if(num==0){
				return "Non ci sono messaggi! Godetevi il vostro giorno!";
			}else if(num==1){
				return "Hai 1 messaggio di testo attesa.";
			}else{
				return "Avete  "+num+" messaggi di testo attesa.";
			}
			
		//FR
		}else if(currentLang.ToLower().Contains("fr")){
			if(num==0){
				return "Vous n'avez pas de messages!";
			}else if(num==1){
				return "Vous avez 1 message en attente!";
			}else{
				return "Vous avez "+num+" messages en attente!";
			}
		//ENG			
		}else{
			if(num==0){
				return "You have no messages! Enjoy your day!";
			}else if(num==1){
				return "You have 1 Text Message waiting!";
			}else{
				return "You have "+num+" Text Messages waiting!";
			}
		}
				
	}


    //-----------------------------------------------
    // MEMORY USAGE
    //-----------------------------------------------
	
	public void MemoryIsLow(){
		/*
		EnableOnlyOneItem(2, backgroundAnimations);
		EnableOnlyOneItem(2, crossItems);
		
		fire.SetActive(false);*/
		lowMemory = true;
		Debug.Log("OnTrim --> MemoryIsLow() called !");
	}
}
