using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using JsonFx.Json;
/******************************************************************
 * EventManager (singleton) 
 * ---------------
 * function :  when event from java are fire like touch or swipe , every event are displayed to all 
 * registered callback function by a specific class pass as params(EventBase) to all callbacks
 * 
 * How to use : put this gameObject on InitScene named Gameobject and if InitScene component is absent we add it to gameobject.
 * 
 * ***************************************************************/
public class EventBase
{

    [JsonName("eventName")]
    public string EventName { get; set; }

    [JsonName("eventParam")]
    public Dictionary<string, object> Param { get; set; }

    public T GetParamValue<T>(string name, T defaultValue)
    {
        if (this.Param == null) return defaultValue;

        object value;
        if (this.Param.TryGetValue(name, out value) && value is T)
        {
            return (T)value;
        }
        try
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }
        catch (InvalidCastException)
        {
            return defaultValue;
        }

    }

    public bool TryGetParamValue<T>(String name, out T value)
    {
        object v;
        if (this.Param != null && this.Param.TryGetValue(name, out v) && v is T)
        {
            value = (T)v;
            return true;
        }
        try
        {
            value = (T)Convert.ChangeType(v, typeof(T));
            return true;
        }
        catch (InvalidCastException)
        {
            value = default(T);
            return false;
        }
    }
}
public class EventManager : SingleBehaviour<EventManager>
{
    //Reset is called when you put this script on a gameobject
    void Reset()
    {
        // be sure that when we put this script on our INITSCENE CTLR object we happend EventManager too.
        if (gameObject.GetComponent<InitScene>() != null)
            gameObject.AddComponent<InitScene>();
    }
    // this is a public dictionnary that allow you to store and look every possible events from java bridge
    public Dictionary<string, string> EVENT_LIST = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

    public delegate void sceneEventReceiver(string data);

    private event sceneEventReceiver globalEvent;
    private Dictionary<string, List<Action<EventBase>>> handlers = new Dictionary<string, List<Action<EventBase>>>(StringComparer.InvariantCultureIgnoreCase);
    void Awake()
    {
        // Set events list available
        EVENT_LIST.Add("touch_start", "touch_start");
        EVENT_LIST.Add("touch_end", "touch_end");
        EVENT_LIST.Add("swipe", "swipe");
        EVENT_LIST.Add("double_touch", "touch_double");
    }

    /****************************
     * VOID DispatchEvent (String data) : event fire from java to unity scene.
     *  data : stringified JSON.
     *  return :none
     * **************************/
    public void DispatchEvent(string data)
    {
        Debug.Log("-- DispatchEvent() ------------------- ");
        Debug.Log("data = " + data);
        // desirialize and store data from JAVA
        EventBase myEvent = JsonReader.Deserialize<EventBase>(data);

        if (this.handlers.ContainsKey(myEvent.EventName))
        {
            // get the targeted list from our dictionary of Lists
            List<Action<EventBase>> handlers = this.handlers[myEvent.EventName];
            Debug.Log("search event name = " + myEvent.EventName);
            // trigger each callback inside our current selected list
            foreach (Action<EventBase> action in handlers)
            {
                action(myEvent);
            }
        }
    }
    /****************************
     * VOID RegisterHandler (String eventName , Action<EventBase> handler) : register a call back for a specific event.
     *  eventName: string event name.
     *  handler : callback function call on DispatchEvent execution.
     *  return :none
     * **************************/
    public void RegisterHandler(string eventName, Action<EventBase> handler)
    {
        List<Action<EventBase>> h;
        if (String.IsNullOrEmpty(eventName) || handler == null)
        {
            Debug.Log("RegisterHandler : ERROR eventName null or empty");
            return;
        }
        if (handlers.ContainsKey(eventName))
        {
            h = handlers[eventName];
            h.Add(handler);
        }
        else
        {
            h = new List<Action<EventBase>>();
            h.Add(handler);
            handlers.Add(eventName, h);
        }

    }
    /****************************
     * VOID UnregisterHandler (String eventName , Action<EventBase> handler) : Unregister a callback for a specific event.
     *  eventName: string event name.
     *  handler : callback function call on DispatchEvent execution.
     *  return :none
     * **************************/
    public void UnregisterHandler(string eventName, Action<EventBase> handler)
    {
        List<Action<EventBase>> handlers = this.handlers[eventName];
        if (handlers != null && handlers.Contains(handler))
        {
            handlers.Remove(handler);
        }
    }
}
