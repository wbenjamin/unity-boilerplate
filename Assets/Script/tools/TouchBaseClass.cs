using UnityEngine;
using System.Collections;

public abstract class TouchBaseClass : MonoBehaviour
{
    public virtual void TriggerEvent()
    {

    }
    public virtual void MouseDoubleEvent()
    {

    }
    public virtual void ReloadEvent(int cam_)
    {

    }
	public virtual void RayCaster (Vector3 pos_){
        
    }
}