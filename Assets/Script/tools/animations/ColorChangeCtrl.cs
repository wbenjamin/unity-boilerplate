using UnityEngine;
using System.Collections;
/******************************************************************
 * ColorChangeCtrl 
 * ---------------
 * function :  when this gameobject collider is hit by a touch that will pass red colors from objectPool to 
 * a red color smooth and after a certain time return to normal material color
 * 
 * How to use : put this script on game object that you want to be the root and 
 * controler of child objects you will want to change material color on touch
 * 
 * ***************************************************************/
public class ColorChangeCtrl : TouchBaseClass {
    //Reset is called when you put this script on a game object
    void Reset()
    {
        // check if box collider is present the current gameobject and if not happen a new one on it
        if (gameObject.GetComponent<BoxCollider>() != null)
            gameObject.AddComponent<BoxCollider>();
    }
    public GameObject[] objectPool;
    private Color[] objectPoolColor;
    private bool ColorLerp;
    private bool colorLerpReturnNormal;
    private float red;
    public float redDelta;
    // Use this for initialization
    void Awake()
    {
        objectPoolColor = new Color[objectPool.Length];
        
        colorLerpReturnNormal = false;
        ColorLerp = false;

        red = 0;
        for (int index = 0 ; index < objectPool.Length ; index++)
        {
            objectPoolColor[index] = objectPool[index].renderer.sharedMaterial.color;
        }
    }
	void Start () {
	}
    public override void TriggerEvent()
    {
        OnMouseDown();
    }
	void OnMouseDown(){
        if (!ColorLerp && !colorLerpReturnNormal )
        {
            ColorLerp = true;
            red += redDelta;
        }
        //Debug.Log("-----------------------------------------------------");
        //Debug.Log(" red color :" + red + " colorLerp:" + ColorLerp + " premier if valid ?" + (ColorLerp && !colorLerpReturnNormal));
        //Debug.Log("-----------------------------------------------------");
    }


	// Update is called once per frame
	void Update () {
        
        if (ColorLerp && !colorLerpReturnNormal)
        {
            if (LerpColor(red)){
               return ;
            }
            else {
                ColorLerp = false;
                colorLerpReturnNormal = true;
                red = 0;
                return;
            }
        }
        else if (!ColorLerp && colorLerpReturnNormal)
        {
            if (LerpColor(objectPoolColor[5].r))
            {
                return;
            }
            else
            {
                colorLerpReturnNormal = false;
                ColorLerp = false;
            }
        }

	}

    bool LerpColor(float value)
    {
        int numberOfobjectPoolOk = 0;
        for (int i = 0; i < objectPool.Length; i++)
        {

            if (!Mathf.Approximately(objectPool[5].renderer.sharedMaterial.color.r, value))
            {
                //Debug.Log("color values:" + objectPool[i].renderer.sharedMaterial.color.r + " = " + value + " " + Mathf.Approximately(objectPool[i].renderer.material.color.r, value));
                objectPool[i].renderer.material.color = Color.Lerp(objectPool[i].renderer.material.color, new Color(value, objectPoolColor[i].g, objectPoolColor[i].b), Time.deltaTime * 5);
            }
            else if (Mathf.Approximately(objectPool[5].renderer.material.color.r, value))
            {
                return false;
            }
        }
        return true;

       // Debug.Log("numberOF objectPool color ok :"+numberOfobjectPoolOk);
        
    }
}
