using UnityEngine;
using System.Collections;

public class MultiSkyBox : MonoBehaviour {

   // public Material[] Skybox;
	public string[] SkyboxFileName;
	private int currentIndex = -1;
	public GameObject userBg;
	
	public void SetSkyBox (int index) {
        //RenderSettings.skybox = Skybox[index];
		if(currentIndex != index)
		{
			if(index == 5){
				string fileTexture = getPrefPath("wallpaper_photo_asset","");
				Debug.Log("fileTexture = "+fileTexture);
				if(fileTexture.Length!=0){
					StartCoroutine(LoadImages(fileTexture,userBg));
					userBg.renderer.enabled = true;
					currentIndex = index;
				}else{
					userBg.renderer.enabled = false;
				}
			}else{
				StartCoroutine(LoadImageAssets(SkyboxFileName[index]));
				userBg.renderer.enabled = false;
				currentIndex = index;
				
			}
			
		}
    }
	
	private IEnumerator LoadImageAssets(string pFile){
		
		
		string assetFiles = "jar:file://" + Application.dataPath + "!/assets/"  + pFile;
		//string assetFiles = pFile;
		Debug.Log("# GET ASSET FILES #################");
		Debug.Log("--->assetFiles= "+assetFiles);
		var www = new WWW(assetFiles);
		yield return www;
		// www.LoadImageIntoTexture(lObject.renderer.material.GetTexture("_MainTex") as Texture2D);
		if(www.size>0)
		{
				Debug.Log("--->assetFileswww.size "+www.size);
			
			
				 SkyboxManifest manifest = new SkyboxManifest(www.texture, www.texture,www.texture,www.texture, www.texture,www.texture);
		         Material material = CreateSkyboxMaterial(manifest);
		         SetSkybox(material);
				/*
				Material lMat = RenderSettings.skybox;
				Debug.Log("lMat = "+lMat);
				lMat.SetTexture("_MainTex",www.texture);
				*/
		}
		
	}
	/*****************************
	 * PERSONNAL PIC LOADER
	 ****************************/
	
	string getPrefPath(string pNameParam, string pDefaultValue) {

        if (PlayerPrefs.HasKey(pNameParam))
        {
            return "file://"+PlayerPrefs.GetString(pNameParam);
           
        }
		
        return pDefaultValue;
    }

	
	private IEnumerator LoadImages(string pFile,GameObject pObject){
		Debug.Log("LoadImages() --->pFile= "+pFile);
		var www = new WWW(pFile);
		yield return www;
		// www.LoadImageIntoTexture(lObject.renderer.material.GetTexture("_MainTex") as Texture2D);
		if(www.size>0){
			Debug.Log("LoadImages() image is OK!");
		if(pObject!=null) pObject.renderer.material.SetTexture("_MainTex",www.texture);
		
		
		//Application.CaptureScreenshot("Screenshot.png");
		
		/*GameObject lObject2 = GameObject.Find("Main Camera");
		CameraAnimation lScript = (CameraAnimation) lObject2.GetComponent(typeof(CameraAnimation));
		lScript.TriggerScreenCapture(screenCaptureCameraPos,screenCaptureFileName);*/
		}
	}
	
	
	/*****************************
	 * SKYBOX CREATORE
	 ****************************/
	
	public static Material CreateSkyboxMaterial(SkyboxManifest manifest)
     {
         Material result = new Material(Shader.Find("RenderFX/Skybox"));
         result.SetTexture("_FrontTex", manifest.textures[0]);
         result.SetTexture("_BackTex", manifest.textures[1]);
         result.SetTexture("_LeftTex", manifest.textures[2]);
         result.SetTexture("_RightTex", manifest.textures[3]);
         result.SetTexture("_UpTex", manifest.textures[4]);
         result.SetTexture("_DownTex", manifest.textures[5]);
         return result;
     }
 
     public Texture2D[] textures;
 
     void SetSkybox(Material material)
     {
         GameObject camera = Camera.main.gameObject;
         Skybox skybox = camera.GetComponent<Skybox>();
         if (skybox == null)
             skybox = camera.AddComponent<Skybox>();
         skybox.material = material;
     }
 }
 
 public struct SkyboxManifest
 {
     public Texture2D[] textures;
 
     public SkyboxManifest(Texture2D front, Texture2D back, Texture2D left, Texture2D right, Texture2D up, Texture2D down)
     {
         textures = new Texture2D[6]
         {
             front,
             back,
             left,
             right,
             up,
             down
         };
     }
}
