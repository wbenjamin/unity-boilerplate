using UnityEngine;
using System.Collections;

/******************************************************************
 * RotateAnimation 
 * ---------------
 * function :  allow you to rotate a object on it self and change or not it's rotation direction 
 * by swipe if swipeIsAllow set to true.
 * 
 * How to use : put this script on game object that you want to rotate 
 * 
 * ***************************************************************/
public class RotateAnimation : MonoBehaviour {

    // current speed rotation
    public int speedRotation;
    //allow or not swipe during to change rotation of rotation direction
    [HideInInspector]
    public bool swipeIsAllow;

    // default initial object angle backup
    private Quaternion defaultAngle;
    //angle during rotation
    private Vector3 currentAngle;
    // current swipe Velocity value
    private float SwipeVelocity;
    //current swipe Direction 
    private string SwipeDirection;
	
    // At awake of scene we backup initial gameobject rotation value 
	void Awake () {
        defaultAngle = transform.rotation;
        currentAngle = new Vector3(defaultAngle.x, defaultAngle.y, defaultAngle.z);
	}

    // at start we register our event inside our global event manager
    void Start()
    {
        EventManager.instance.RegisterHandler(EventManager.instance.EVENT_LIST["swipe"], SwipeCmd);
    }
    // swipe event callback
    void SwipeCmd(EventBase e)
    {
        if (SwipeVelocity < 7f && SwipeVelocity > -7f && e.GetParamValue("Side", "") != "down" && swipeIsAllow)
        {
            SwipeVelocity -= e.GetParamValue("Velocity", 1f)/1000;
            SwipeDirection = e.GetParamValue("Side", "");
        }
        
    }
    void OnDisable() {
        InitObjectAngle();
    }
    void OnEnable() {
        InitObjectAngle();
    }

    void InitObjectAngle() {
        SwipeVelocity = 1;
        SwipeDirection = null;
        transform.rotation = defaultAngle;
    }

	void LateUpdate () {
        if (swipeIsAllow)
        {
             if (SwipeDirection == "right" && SwipeVelocity < 1.0f)
            {
                SwipeVelocity += Time.deltaTime * 5;
            }
            else if(SwipeDirection=="left" && SwipeVelocity > 1.0f){
                SwipeVelocity -= Time.deltaTime * 5;
            }
            else if (!string.IsNullOrEmpty(SwipeDirection) && SwipeVelocity == 1f)
            {
                SwipeDirection = null;
            }
        }
        currentAngle.y = Time.deltaTime * speedRotation * SwipeVelocity;
        transform.Rotate(currentAngle);
	}
}
