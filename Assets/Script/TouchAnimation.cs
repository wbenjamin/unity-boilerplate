using UnityEngine;
using System.Collections;

public class TouchAnimation : MonoBehaviour {
	private Animator animator;
	public GameObject particleEmitter;
	public float timer = 5f;
	public float delay = 2f;
	public bool GoToCamera = false;
	private ParticleSystem particle;
	private bool count = false;
	private float setTimer;
	
	// Use this for initialization
	void Start () {
        EventManager.instance.RegisterHandler("touch_start", OnMouseDown);
		setTimer = timer;
		animator = gameObject.GetComponent<Animator>();
		if(particleEmitter){
			particle = particleEmitter.particleSystem;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (animator.GetAnimatorTransitionInfo(0).IsUserName("InitState")){
			animator.SetBool("Action", false);
		}
		if(count){
			timer -= Time.deltaTime;
			if(timer<=0){
				count = false;
				timer = setTimer; 
				animator.SetBool("Action", false);
			}
		}
	}

    void OnMouseDown(EventBase e)
    {
		
		animator.SetBool("Action", true);
		if(GoToCamera)
			SendMessage("ChangeTarget", Camera.main.transform.position);		
				
		if(particle)
			particle.Play();
		
		count = true;
		
	}

}
