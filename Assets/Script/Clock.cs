using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace SIEN
{
	public class DateTime
	{
		public int second;
		public int minute;
		public int hour;
		public int minutes24;
		
		public DateTime (int hour_, int minute_, int second_)
		{
			second = second_ ;
			minute = minute_ ;
			hour = hour_ ;
			
			minutes24 = minute + hour*60;
		}
		static public DateTime Now ()
		{
			
			System.DateTime now = System.DateTime.Now;
			DateTime dt = new DateTime (now.Hour, now.Minute, now.Second);
			return dt ;
		}
		public void Update ()
		{
			//
			
			
			System.DateTime now = System.DateTime.Now;
			hour = now.Hour;
			minute = now.Minute;
			second = now.Second;
			
			minutes24 = minute + hour*60;
			
			//CultureInfo.CurrentCulture.ClearCachedData();
			//System.Threading.Thread.CurrentThread.CurrentCulture.ClearCachedData();
		}
	}
}

//[RequireComponent (typeof (Animation))]
//[RequireComponent (typeof (AudioSource))]
//[RequireComponent (typeof (Collider))]
[AddComponentMenu ("SIEN/Default/Clock")]
public class Clock : MonoBehaviour
{
	private SIEN.DateTime time ;
	
	public Transform minutes;
	public Transform hours;
	public Transform seconds;
    public bool UseSeconds=true;
//	private int _;
	
//	Quaternion secQ ;
	
	void Awake ()
	{
		//System.DateTime now = System.DateTime.Now ();
		time = SIEN.DateTime.Now();
	}
    void OnEnable()
    {
        time = SIEN.DateTime.Now();
    }


	void Update ()
	{
		time.Update();
//		seconds.Rotate(new Vector3(0,0,-Time.deltaTime));
        if (UseSeconds) {
            Quaternion secQ = Quaternion.Euler(new Vector3(0, 0, -time.second * 6));
            seconds.localRotation = secQ;
        }

        Quaternion minutesQ = Quaternion.Euler(new Vector3(0, -time.minute * 6, 0));
		minutes.localRotation = minutesQ ;
        
        Quaternion hourQ = Quaternion.Euler(new Vector3(0, 88 - time.minutes24 * 0.5f, 0));
		hours.localRotation = hourQ ;
	}
}
