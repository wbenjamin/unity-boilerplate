using UnityEngine;
using System.Collections;

public class AutoRotate : MonoBehaviour {
	public bool LockX;
    public bool LockY;
    public bool LockZ;
	public float speed = 1f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!LockX)
			transform.Rotate(Time.deltaTime*speed,0,0);
		if(!LockY)
			transform.Rotate(0, Time.deltaTime*speed,0);
		if(!LockZ)
			transform.Rotate(0, 0, Time.deltaTime*speed);
	
	}
}
