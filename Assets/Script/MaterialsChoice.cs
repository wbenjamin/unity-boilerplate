using UnityEngine;
using System.Collections;

public class MaterialsChoice : MonoBehaviour {

	// Use this for initialization
    public Material[] materialsList;

	void Start () {
	
	}

    public void setMaterial (int index)
    {
        if (index < materialsList.Length)
        {
            renderer.material = materialsList[index];
        }
    }

    public int materialsListLenght
    {
        get
        {
            return renderer.materials.Length;
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
