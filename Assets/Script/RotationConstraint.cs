﻿using UnityEngine;
using System.Collections;

public class RotationConstraint : MonoBehaviour
{

	public Transform target ;
	public Transform constraint ;
	public float limitAngle = 90f;
	 public GameObject[] eyes;
	
	private Quaternion origin;
	


	void Start ()
	{
		//origin = target.rotation;
		  foreach (GameObject currentEye in eyes)
            {
                if (currentEye.activeSelf) origin = currentEye.transform.localRotation ;
            }
	}

	void Update ()
	{
		
		
		if (Quaternion.Angle (constraint.localRotation, origin) <= limitAngle)
		{
			
			//if (smooth)
			//target.transform.rotation = constraint.rotation ;
			
			  foreach (GameObject currentEye in eyes)
            {
                //if (currentEye.activeSelf) currentEye.transform.rotation = constraint.rotation ;
				//currentEye.transform.rotation  = Quaternion.Slerp (currentEye.transform.rotation,constraint.rotation,0.01f) ;
				//if (currentEye.activeSelf) currentEye.transform.localRotation = constraint.localRotation ;
				if (currentEye.activeSelf) currentEye.transform.localRotation =Quaternion.Slerp (currentEye.transform.localRotation,constraint.localRotation,0.7f) ;
            }
//			else
//				transform.rotation = tempTransform ;
		}
	}
}
