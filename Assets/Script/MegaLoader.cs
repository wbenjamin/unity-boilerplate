using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

//[RequireComponent (typeof (Animation))]
//[RequireComponent (typeof (AudioSource))]
//[RequireComponent (typeof (Collider))]
[AddComponentMenu ("SIEN/Default/MegaLoader")]

public class MegaLoader : MonoBehaviour
{
	
	public string nameScene;
	public bool isActive = true;
	public bool needFBConnect = false;
	public GameObject fbBg;
	
	void Update ()
	{
	
	}
	
	void Start ()
	{
		Caching.CleanCache ();
		
		//ApplyVisibilityGroup(fbBg ,needFBConnect);
		
		// WHAT SCENE WE WANT???
		if(isActive){
			//IS THE PERSON LOGGED ON FB?
			//if(!needFBConnect){
				StartCoroutine (Load ());
			//}
		}
	}

	int getPrefInt(string pNameParam, int pDefaultValue) {

        if (PlayerPrefs.HasKey(pNameParam))
        {
            return PlayerPrefs.GetInt(pNameParam);
        }
		
        return pDefaultValue;
    }
	
	void tryToLoad(){
		Debug.Log("TRY TO LOAD !!!!");
		string isConnect = getPrefString("wallpaper_social_connect","false");
		Debug.Log("tryToLoad() isConnect= "+isConnect);
			if(isConnect.Contains("true")){
				ApplyVisibilityGroup(fbBg ,false);
				StartCoroutine (Load ());
			}else{
				ApplyVisibilityGroup(fbBg ,true);
			}
	}
	
	public static string getPrefString(string pNameParam, string pDefaultValue) {

        if (PlayerPrefs.HasKey(pNameParam))
        {
            return PlayerPrefs.GetString(pNameParam);
           
        }
		
        return pDefaultValue;
    }
	
	
	
	IEnumerator Load()
	{
		AsyncOperation aSyncOp = Application.LoadLevelAsync(nameScene);
		yield return aSyncOp;
	}
	
	void ApplyVisibilityGroup(GameObject pGObject,bool pBool){
	        foreach (Transform tran in  pGObject.transform)
	        {
				if(tran.renderer != null) tran.renderer.enabled = pBool;
	        }
			if(pGObject.transform.renderer) pGObject.transform.renderer.enabled = pBool;
	}

//	void Update ()
//	{
//		
//	}
}
