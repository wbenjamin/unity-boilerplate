using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class SoundAnimation
{
	public string name ;
	public AnimationClip animationClip ;
	public AudioClip audioClip ;
}

[RequireComponent (typeof (Animation))]
[RequireComponent (typeof (Collider))]
[AddComponentMenu ("SIEN/Interaction/Play")]
public class PlayOnClick : TouchBaseClass //MonoBehaviour
{
//	public SoundAnimation [] soundAnimation ;
	public List<GameObject> particleEmitters;
	public List<GameObject> particleSystems;
	
	public List<GameObject> stuffToAnimate;
	private List<ParticleEmitter> _particles;
	private List<ParticleSystem> _systems;
	
	public List<GameObject> particleSystemsLast;
	private List<ParticleSystem> _systemsLast;
	
	public bool triggerOwnAnim = true;
	public bool ClearParticulesOnClick = true;
	
	public bool animationWorkOnce = false;
	public int myCam = 1;
	private bool already = false;
	
	public GameObject lastStuffToAnimate;
	public List<GameObject> lastStuffToEmit;
	
	//public List<GameObject> lastStuffSystemToEmit;
	
	private List<ParticleEmitter> _particlesLast;
	public float timeRemaining = 60f;
	private float timeLeft = 0;
	private bool timeElapsed = false;
	
	public float durationNormalAnim = 1.5f;
	private float timeLeftTotal = 0;
	private bool isPlaying = false;
	
	public float durationNormalEmitter = 1.5f;
	private float timeLeftEmitter = 0;
	private bool isPlayingEmitter = false;
	//----------------------------
	public GameObject stuffToDisappear;
	
	private int CurrentCam =1;
	
	
//	private Animation _anim ;
	void Awake ()
	{
		
		/* PARSING PARTICLES LIST */
		
		_particles = new List<ParticleEmitter>();
		foreach (GameObject g in particleEmitters){
			if(g.GetComponent<ParticleEmitter>() != null) _particles.Add(g.GetComponent<ParticleEmitter>());
		}

		foreach (ParticleEmitter particule in _particles){
			//particule.Stop();
			particule.ClearParticles();
		}
		
		_particlesLast = new List<ParticleEmitter>();
		foreach (GameObject g in lastStuffToEmit){
			if(g.GetComponent<ParticleEmitter>() != null) _particlesLast.Add(g.GetComponent<ParticleEmitter>());
		}
		
		foreach (ParticleEmitter particule in _particlesLast){
			//particule.Stop();
			particule.ClearParticles();
		}
		
		//------------------------------
		//PARTICULES SYSTEM
		_systemsLast = new List<ParticleSystem>();
		foreach (GameObject gs2 in particleSystemsLast){
			if(gs2.GetComponent<ParticleSystem>() != null) _systemsLast.Add(gs2.GetComponent<ParticleSystem>());
		}
		
		_systems = new List<ParticleSystem>();
		foreach (GameObject gs in particleSystems){
			if(gs.GetComponent<ParticleSystem>() != null) _systems.Add(gs.GetComponent<ParticleSystem>());
		}
		
//		_anim = GetComponent<Animation>();
	}
	
//	void Start ()
//	{
//		
//	}
//	
	void Update ()
	{
		if(timeLeft >= 0){
			timeLeft -= Time.deltaTime;
		}else{
			if(timeElapsed){
				foreach (ParticleEmitter particule in _particlesLast){
					particule.Emit();
				}
				
				//ANIMATE SYSTEM
				foreach (ParticleSystem system2 in _systemsLast){
					system2.Play();
				}
			}
			timeElapsed = false;
		}
			/* IS AN ANIMATION PLAYING */
		if(timeLeftTotal >= 0){
			timeLeftTotal -= Time.deltaTime;
		}else{
			if(isPlaying){
				isPlaying = false;
				/*
				foreach (GameObject gameObject in stuffToAnimate){
				//if(!gameObject.animation.isPlaying){
					rewindAnim(gameObject.animation,false);
				//}
				}*/
			} 
		}
	}
	
	public override void MouseDoubleEvent(){
		//OnMouseUp();
	}
	
	public override void TriggerEvent ()
	{
		OnMouseDown () ;
	}
	
	void OnMouseDown ()
	{
		Debug.Log("OnMouseDown()");
		if(!isPlaying && CurrentCam == myCam){
			if (triggerOwnAnim )
			{
				if(!already){
					 foreach(AnimationState animState in animation )
				       {
				            animState.time = 0f;
				            animState.speed = 1f;
						    animation.Play();
				       }
					
					if(animationWorkOnce) already = true;
					isPlaying = true;
					//timer if there is an ending animation
					//if(lastStuffToAnimate){
						timeLeft = timeRemaining;
						timeElapsed = true;
					//}
				}
			}
			
			foreach (ParticleEmitter particule in _particles){
				if(ClearParticulesOnClick) particule.ClearParticles();
			}
			
			foreach (ParticleEmitter particule in _particles){
				particule.Emit();
			}
			
			foreach (ParticleSystem sys in _systems){
				if(sys != null)sys.Play();
			}
			
			/* Trigger animations */
			foreach (GameObject gameObject2 in stuffToAnimate){
				//if(!gameObject.animation.isPlaying){
					//rewindAnim(gameObject2.animation,true);
					gameObject2.animation.Rewind();
					gameObject2.animation.Play("Asteroid_Animation");
				//}
			}
			
			/* STUFF TO DISAPPEAR */
			if(stuffToDisappear != null) stuffToDisappear.transform.renderer.enabled = false; 
			
			if(animationWorkOnce)already = true;
			timeLeft = timeRemaining;
			timeElapsed = true;
			
			isPlaying = true;
			timeLeftTotal = durationNormalAnim;
		}
	}
	
	/* DISABLE ALL*/
	public override void ReloadEvent (int currentCam_)
	{
		CurrentCam = currentCam_;
		
		
		foreach (GameObject gameObject2 in stuffToAnimate){
				//if(!gameObject.animation.isPlaying){
					//rewindAnim(gameObject2.animation,true);
					gameObject2.animation.Rewind();
				//}
			}
		
		if(animationWorkOnce){
			reloadAnim(currentCam_) ;
		}
		if(stuffToDisappear != null) stuffToDisappear.transform.renderer.enabled = true; 
	}
	
	public override void RayCaster(Vector3 pos_){
		//OnMouseUp();
	}
	
	public void reloadAnim(int currentCam_){

			

       foreach(AnimationState animState in animation )
       {
	        animState.time = 0f;
	        animState.speed = 0f;
	        animation.Play();
			already = false;
       }

	}
	
	/* ANIMATION RELOADER */
	public void rewindAnim(Animation animation_,bool pValue){

		 foreach(AnimationState animState in animation_)
       {
			if(!pValue){
		        animState.time = 0f;
		        animState.speed = 0f;
		        animation.Play();
				already = false;
			}else{
				animState.time = 0f;
				animState.speed = 1f;
				already = true;
			}
       }
	}
	
	
	
}
