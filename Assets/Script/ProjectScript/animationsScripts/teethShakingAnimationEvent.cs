using UnityEngine;
using System.Collections;

public class teethShakingAnimationEvent : TouchBaseClass{

    private Transform savePos;



    public override void TriggerEvent()
    {
        OnMouseDown();
    }

	// Use this for initialization
    void Awake()
    {
        savePos = transform;
    }
    void Start () {
	
	}
    void OnMouseDown()
    {
        if (!animation.isPlaying)
        {
            animation.Play();
        }
    }
    void resetAnimation()
    {
        transform.position = savePos.position;
        transform.rotation = savePos.rotation;
    }
	// Update is called once per frame
}
