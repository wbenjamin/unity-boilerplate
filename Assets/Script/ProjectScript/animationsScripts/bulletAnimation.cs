using UnityEngine;
using System.Collections;

public class bulletAnimation : TouchBaseClass
{
    public GameObject bulletMesh;
	private float time = 0;
	public float  timeAppearance = 3f;
	
	
	// Use this for initialization
	void Start () {
	    
	}
	
	void Update(){
		if(time > 0){
			time -= Time.deltaTime;
			if(time<=0)bulletMesh.SetActive(false);
		}
	}
	
    public override void TriggerEvent()
    {
        //OnMouseDown();
    }
	
	public override void MouseDoubleEvent()
	{
		bulletMesh.SetActive(true);
		time = timeAppearance;
	}
	
    void OnMouseDown()
    {
        //Debug.Log("click bullet : " + bulletMesh.activeSelf);
        //bulletMesh.SetActive((bulletMesh.activeSelf)?false:true);
    }
	
}
