using UnityEngine;
using System.Collections;

public class clockAnimationEvent : TouchBaseClass
{
    private bool isInFront;
	private float timeLastTouch = 0;
	private float lastTouchMin = 3f;
	public string animationName = "clockBlackMove";
	
    public override void TriggerEvent()
    {
		//put in back
         if(isInFront && !animation.isPlaying)
        {
            isInFront = false;
            resetPos();
			timeLastTouch =0;
        }
		
    }
    void Awake()
    {
        isInFront = false;
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timeLastTouch += Time.deltaTime;
	}
	
	void OnMouseDown(){
		TriggerEvent();
		MouseDoubleEvent();
	}
	
	
    public override void MouseDoubleEvent()
    {

		//PUT IN FRONT
		 if(!isInFront && !animation.isPlaying){
			if(timeLastTouch > lastTouchMin){
            isInFront = true;
           restartPos();
			}
        }
    }
	/*
    IEnumerator resetPos()
    {
        yield return animation.Play();
        animation.Stop();
    }
	 */
	
	void resetPos()
    {
	    animation.Play(animationName);
		animation[animationName].speed = -1.25f;
		//animation[animationName].time = 0;
		
    }
	
	void restartPos()
    {
	    animation.Play(animationName);
		animation[animationName].speed = 1;
		animation[animationName].time = 0;
		
    }
}
