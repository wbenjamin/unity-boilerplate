using UnityEngine;
using System.Collections;

public class skullHeadAnimation : TouchBaseClass {
	
    public GameObject skullFireParticles;
	public float timeParticule = 1f;
	private float timePlayed = 0;
	
    
	// Use this for initialization
	void Start () {
		
	}
	
	void Update(){
		
		if(!getPrefBool("wallpaper_use_head_fire", false))
		{
			timePlayed += Time.deltaTime;
			
			if(timePlayed >= timeParticule)
			{
				skullFireParticles.particleSystem.Stop();
			}
		}
	}
	
    public override void TriggerEvent()
    {
        //OnMouseDown();
    }
	
    public override void MouseDoubleEvent()
    {
		if(!getPrefBool("wallpaper_use_head_fire", false))
		{
	       	skullFireParticles.SetActive((skullFireParticles.activeSelf) ? false : true);
	       	skullFireParticles.particleSystem.Play();
			timePlayed = 0;
		}
    }
	
    void OnMouseDown()
    {
		/*
		skullFireParticles.SetActive((skullFireParticles.activeSelf) ? false : true);
       skullFireParticles.particleSystem.Play();
		timePlayed = 0;*/
    }
	
	 bool getPrefBool(string pNameParam, bool pDefaultValue)
	{
        string lTempBool;

        if (PlayerPrefs.HasKey(pNameParam))
        {
            lTempBool = PlayerPrefs.GetString(pNameParam);
            if (lTempBool == "true")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return pDefaultValue;
    }
}
