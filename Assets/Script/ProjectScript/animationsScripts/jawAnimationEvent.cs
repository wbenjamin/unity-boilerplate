using UnityEngine;
using System.Collections;

public class jawAnimationEvent : TouchBaseClass{

	// Use this for initialization
    string SwipeDirection;
    void Start()
    {
        EventManager.instance.RegisterHandler(EventManager.instance.EVENT_LIST["swipe"], SwipeCmd);
    }
     void SwipeCmd(EventBase e)
    {
        SwipeDirection = e.GetParamValue("Side", "");
        if(SwipeDirection == "down"&& !animation.isPlaying) animation.Play();
    }
    public override void TriggerEvent()
    {
        OnMouseDown();
    }
    
    void jawClosed() {
        animation.Stop();
        //StartCoroutine(playEndAnimation());
    }
    private IEnumerator playEndAnimation()
    {
        yield return new WaitForSeconds(3);
    }
    void OnMouseDown()
    {
        animation.Play();
    }
   
}
