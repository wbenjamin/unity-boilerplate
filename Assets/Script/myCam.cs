using UnityEngine;
using System.Collections;


public class myCam : MonoBehaviour {
 
    public float scaleFactor = 5f;
    private float camOffsetFactor = 0.5f;
    private Vector3 newPosition;
 
    void FixedUpdate()
    {
        newPosition = new Vector3((camOffsetFactor - 0.5F) * scaleFactor, transform.position.y, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, newPosition, 0.02F);
    }
 
    public void SetCamOffsetFactor(string offset)
    {
        camOffsetFactor = float.Parse(offset);
    }
	void OnGUI()
	{
	    GUILayout.BeginHorizontal();
	    if(GUILayout.Button("set to 0.0"))
	    {
	        SetCamOffsetFactor("0");
	    }
	    if(GUILayout.Button("set to 0.5"))
	    {
	        SetCamOffsetFactor("0.5");
	    }
	    if(GUILayout.Button("set to 1.0"))
	    {
	        SetCamOffsetFactor("1");
	    }
	    GUILayout.EndHorizontal();
	}
}
 
