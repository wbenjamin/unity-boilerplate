using UnityEngine;
using System.Collections;

public class PositionConstrain : MonoBehaviour {
	public bool LockX;
    public bool LockY;
    public bool LockZ;
	public Transform target;
	
	public float offsetX;
	public float offsetY;
	public float offsetZ;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 p = Vector3.zero;
			if(!LockX)
				p.x = target.position.x+offsetX;
			if(!LockY)
				p.y = target.position.y+offsetY;
			if(!LockZ)
				p.z = target.position.z+offsetZ;
			transform.position = p;
	}
}
