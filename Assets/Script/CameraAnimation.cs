﻿using UnityEngine;
using System.Collections;

public class CameraAnimation : MonoBehaviour {

	//public variables
    public bool useCheckPoints;
    public GameObject[] checkpointsList; //list of predefined camera positions
	public int firstCheckpointIndex = 0; //define the first camera position
	public float cameraSpeed = 4f;
	public float gyroSpeed = 4f;
	public float reachDistance = 1f; //distance to determine when the main camera has reached a checkpoint
	public float reachAngle = 1f;
	public bool useGyroscope = false; //define if the camera control use gyroscope
	public float[] gyroscopeForcesList; //for every checkpoint in the checkpointsList, fix the force of the gyroscope
	public float[] gyroscopeMargesList; //for every checkpoint in the checkpointsList, limit the marge of the gyroscope
	public GameObject cameraLookAtObject; //the objectPool where the main camera is looking at when using gyroscope - used for PARALLAX effect
	public float cameraLookAtDistance = 10f; //define the distance of this objectPool from the main camera
    [HideInInspector]
    public bool cameraSwitchIsActive;
	//private variables
	private int currentCheckpointIndex; //index of the targeted checkpoint 
	private GameObject currentCheckpoint; //targeted checkpoint 
	private Vector3 currentCheckpointPosition; //targeted checkpoint position
	private Vector3 currentCheckpointInitialPosition; //targeted checkpoint initial position (needed for the gyroscope)
	private float currentGyroscopeForce; //current gyroscope force used
	private float currentGyroscopeMarge; //current gyroscope marge used
	private bool checkpointReached = false; //define if the main camera has reached the targeted checkpoint
	private bool checkpointAngleReached = false;
	public float angleSensitivity = 0.02f;

	private float tempInputAccelerationY = 0f; //y input acceleration in the previous frame
	private float tempInputAccelerationX = 0f; //x input acceleration in the previous frame
	
	private bool firstValueGyro = true;


    private string eventDoubleTouchName;
    private string eventTouchStartName;
    private string eventTouchEndName;
    /*******************************
     * objectPool tracking variables
     *******************************/
    //public GameObject[] objectPool;
    //public GameObject eyeConstraint;
    //private Quaternion objectPoolRotationSave;
    //public bool objectPoolTrackingIsActive;
	
    //public GameObject objectPoolLookAtItem;
    private Vector3 targetLookAt;
    private Vector3 targetLookAtOrigin;
	
	public float timeIdleValue = 1;
	private float CamIdleTimer = 0;
	
	
    // Use this for initialization
	void Start () {
        eventDoubleTouchName = EventManager.instance.EVENT_LIST["double_touch"];
        eventTouchStartName = EventManager.instance.EVENT_LIST["touch_start"];
        eventTouchEndName = EventManager.instance.EVENT_LIST["touch_end"];
        /****************************************
         * Event input registration
         ****************************************/
        EventManager.instance.RegisterHandler(EventManager.instance.EVENT_LIST["swipe"], ChangeTargetExt);
        EventManager.instance.RegisterHandler(eventTouchStartName, SendTouchXY);
        EventManager.instance.RegisterHandler(eventDoubleTouchName, SendTouchXY);

        
        
        //set the initial state of the scene
		if (this.useCheckPoints) {
		    this.currentCheckpointIndex = this.firstCheckpointIndex;
		    this.currentCheckpoint = this.checkpointsList [this.currentCheckpointIndex];
		    this.currentCheckpointPosition = this.currentCheckpointInitialPosition = this.currentCheckpoint.transform.position;
        }
        
        if (this.useGyroscope)
        {
			this.currentGyroscopeForce = this.gyroscopeForcesList[this.currentCheckpointIndex];
			this.currentGyroscopeMarge = this.gyroscopeMargesList[this.currentCheckpointIndex];
			this.tempInputAccelerationX = (float)System.Math.Round(Input.acceleration.x, 3);
			this.tempInputAccelerationY = (float)System.Math.Round(Input.acceleration.y, 3);
		}

       //object look at camera section

        //foreach (GameObject currentObject in objectPool)
        //{
        //    if (currentObject.activeSelf)
        //        objectPoolRotationSave = currentObject.transform.localRotation;
        //}
        //if(objectPoolLookAtItem != null){
        //    targetLookAt = objectPoolLookAtItem.transform.position;
        //    targetLookAtOrigin = targetLookAt;
        //}
	}
  
	// Update is called once per frame
	void Update () {
		//listening for user inputs
        
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) {		
			//the user change the target
			if (Input.GetTouch (0).deltaPosition.x < 0) this.ChangeTarget(true);
			if (Input.GetTouch (0).deltaPosition.x > 0)	this.ChangeTarget(false);
		}

		//kayboard inputs
		if (Input.GetKeyDown(KeyCode.RightArrow)){
			this.ChangeTarget(true);
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow)){
			this.ChangeTarget(false);
		}
		
		if(CamIdleTimer>=timeIdleValue){
			targetLookAt = targetLookAtOrigin;
		}else{
			CamIdleTimer += Time.deltaTime;
		}
			
	}

	void LateUpdate() {
    
		//uset the main camera rotation using Slerp to have a traveling path
		// HAS TO WORK THERE between final angle + current angle
		
        if (useCheckPoints)
        {
			transform.rotation = Quaternion.Slerp(this.transform.rotation, this.currentCheckpoint.transform.rotation, Time.deltaTime * 8f);
        
            
                //this.checkpointAngleReached = isAngleReached(this.transform.rotation.eulerAngles); // TO TAKE OFF IF BUGGING
		    //}

		    //is the current targeted checkpoint is not yet reached
		    float speedTranslation = this.cameraSpeed;
		
		    if (!this.checkpointReached) {
			    //if the gyroscope is activated
			    if(this.useGyroscope) {
				    //set the look at objectPool position and rotation using the main camera settings
				    this.cameraLookAtObject.transform.position = this.transform.position + this.transform.forward * this.cameraLookAtDistance;
				    this.cameraLookAtObject.transform.rotation = this.transform.rotation;
				    //use slerp to fix the main camera rotation smoothly
				    this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation( this.cameraLookAtObject.transform.position - this.transform.position ), Time.deltaTime * 4f);
				    speedTranslation = this.gyroSpeed;
			    }

			    //if the distance between the main camera and the targeted checkpoint is lower than reachDistance
                if (this.currentCheckpoint !=null && Vector3.Distance(this.transform.position, this.currentCheckpoint.transform.position) < this.reachDistance)
                {
				    this.checkpointReached = true;
			    }
		    //if the current targeted checkpoint is reached
		    } else {
			    //if the gyroscope is activated
			    if(this.useGyroscope) {
    //				float tempX = this.currentCheckpointPosition.x - (tempAngleX / 5) * this.currentGyroscopeForce;
				    float plusTempInitialX = this.currentCheckpointInitialPosition.z + (this.currentGyroscopeMarge * this.currentGyroscopeForce) / 100;
				    float minusTempInitialX = this.currentCheckpointInitialPosition.z - (this.currentGyroscopeMarge * this.currentGyroscopeForce) / 100;
				
    //				float tempY = this.currentCheckpointPosition.y - (tempAngleY / 5) * this.currentGyroscopeForce;
				    float plusTempInitialY = this.currentCheckpointInitialPosition.y + (this.currentGyroscopeMarge * this.currentGyroscopeForce) / 100;
				    float minusTempInitialY = this.currentCheckpointInitialPosition.y - (this.currentGyroscopeMarge * this.currentGyroscopeForce) / 100;

				    //detect the gyroscope
				    float tempInputY = (float)System.Math.Round(Input.acceleration.y, 2);
				    float tempInputX = (float)System.Math.Round(Input.acceleration.x, 2);
				
				
				
				    // need to manage sensitivity
				    if(firstValueGyro == false){
					    if(System.Math.Pow(tempInputX - tempInputAccelerationX,2)< System.Math.Pow(angleSensitivity,2)) tempInputX =tempInputAccelerationX;
					    if(System.Math.Pow(tempInputY - tempInputAccelerationY,2)< System.Math.Pow(angleSensitivity,2)) tempInputY =tempInputAccelerationY;
				    }else{
					    tempInputAccelerationX = tempInputX;
					    tempInputAccelerationY = tempInputY;
					    firstValueGyro = false;
				    }
				
				    //calculate difference between previous & current input acceleration
				
				    float diffY = tempInputY - tempInputAccelerationY;
				    float diffX = tempInputX - tempInputAccelerationX;

				    //change the camera position if necessary
				    if((diffY > 0 && this.currentCheckpointPosition.y <= plusTempInitialY) || (diffY < 0 && this.currentCheckpointPosition.y >= minusTempInitialY)) {
					    this.currentCheckpointPosition.y += diffY * this.currentGyroscopeForce * Time.deltaTime * 10;
				    }

				    if((diffX > 0 && this.currentCheckpointPosition.z <= plusTempInitialX) || (diffX < 0 && this.currentCheckpointPosition.z >= minusTempInitialX)) {
					    this.currentCheckpointPosition.z += diffX * this.currentGyroscopeForce * Time.deltaTime * 10;
				    }

				    //set previous input acceleration
				    tempInputAccelerationY = tempInputY;
				    tempInputAccelerationX = tempInputX;

				    //update main camera rotation with slerp
				    this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation( this.cameraLookAtObject.transform.position - this.transform.position ), Time.deltaTime * 4f);
			    }
		    }
		    //update main camera position with slerp
		    this.transform.position = Vector3.Slerp(this.transform.position, this.currentCheckpointPosition, Time.deltaTime * speedTranslation);

            // set objectPool look at the main camera ,if screen is not touch 

            //if (objectPoolTrackingIsActive)
            //    eyeConstraint.transform.LookAt(objectPoolLookAtItem.transform);

            //if (objectPoolLookAtItem != null)
            //    objectPoolLookAtItem.transform.localPosition = Vector3.Slerp(objectPoolLookAtItem.transform.localPosition, targetLookAt, Time.deltaTime * 10);
        }
    }

   
	public void ChangeTargetExt(EventBase e){
		
		
        double velocity = e.GetParamValue("Velocity", 0d);
        string side;
		
		
		
        if (e.TryGetParamValue("Side", out side))
        {
            Debug.Log(string.Format("side found: {0}", side));
            Debug.Log(string.Format("velocity found: {0}", velocity));
        }
		
		
		Debug.Log("-- ChangeTargetExt() ------------------- ");
		Debug.Log("e.EventName = "+e.EventName);
		Debug.Log("side = "+side);
		Debug.Log("velocity = "+velocity);
		
        if (side != "down")
        {
            if (side == "left"){
                ChangeTarget(true);
            }
            else{
                ChangeTarget(false);
            }
        }
        return;
	}

	//function to set checkpoint
	public void ChangeTarget(bool toTheRight) {
		//if the current targeted checkpoint is reached, we can set another checkpoint
		if (this.checkpointReached) {
			//index has not been changed yet
			bool isChanged = false;
			//if the camera needs to be moved to the right, set currentCheckpointIndex
            if (cameraSwitchIsActive)
            {

                if (toTheRight) {
				    //check if it is not the last predefined camera position
				    if (this.currentCheckpointIndex + 1 < this.checkpointsList.Length) {
					    this.currentCheckpointIndex++; 
					    isChanged = true;
				    }	
			    //if the camera needs to be moved to the left, set currentCheckpointIndex
			    } else {				
				    //check if it is not the first predefined camera position
				    if (this.currentCheckpointIndex - 1 >= 0) {
					    this.currentCheckpointIndex--;
					    isChanged = true;
				    }
			    }
            
            }
            else
            {
                if (toTheRight)
                {
                    //check if it is not the last predefined camera position
                        
                    //if the camera needs to be moved to the left, set currentCheckpointIndex
                }
                else
                {
                    //check if it is not the first predefined camera position
                    if (this.currentCheckpointIndex - 1 >= 0)
                    {
                        //this.currentCheckpointIndex--;
                        isChanged = false;
                    }
                }
            }
			//if the index has been changed
			if(isChanged) {
				
				targetLookAt = targetLookAtOrigin;
				
				//set the new checkpoint settings
				this.currentCheckpoint = this.checkpointsList[this.currentCheckpointIndex];
				this.currentCheckpointPosition = this.currentCheckpointInitialPosition = this.currentCheckpoint.transform.position;
                //this.currentGyroscopeForce = this.gyroscopeForcesList[this.currentCheckpointIndex];
                //this.currentGyroscopeMarge = this.gyroscopeMargesList[this.currentCheckpointIndex];
				this.checkpointReached = false;
				this.checkpointAngleReached = false;
				
				//camera has stopped */
				TouchBaseClass[] GameCol = GameObject.FindObjectsOfType(typeof(TouchBaseClass)) as TouchBaseClass[];			
				foreach(TouchBaseClass go in GameCol){
					//PLayOnClick playScript = go.GetComponent<PLayOnClick>() as PLayOnClick;
					go.ReloadEvent(currentCheckpointIndex);
				}
			}

			
		}
	}
	
	public void restartCamera(){
		ChangeTargetByIndex(firstCheckpointIndex);
		
	}
	
	public void ChangeTargetByIndex(int pIndex) {
		//if the current targeted checkpoint is reached, we can set another checkpoint
		//if (this.checkpointReached) {
			//index has not been changed yet
			bool isChanged = false;
			
			if (pIndex < this.checkpointsList.Length)
			{
				this.currentCheckpointIndex = pIndex;
				isChanged = true;
			}
			
			//if the index has been changed
			if(isChanged) {
				//set the new checkpoint settings
				this.currentCheckpoint = this.checkpointsList[this.currentCheckpointIndex];
				this.currentCheckpointPosition = this.currentCheckpointInitialPosition = this.currentCheckpoint.transform.position;
                //this.currentGyroscopeForce = this.gyroscopeForcesList[this.currentCheckpointIndex];
                //this.currentGyroscopeMarge = this.gyroscopeMargesList[this.currentCheckpointIndex];
				this.checkpointReached = false;
				this.checkpointAngleReached = false;
				
				//camera has stopped */
				TouchBaseClass[] GameCol = GameObject.FindObjectsOfType(typeof(TouchBaseClass)) as TouchBaseClass[];			
				foreach(TouchBaseClass go in GameCol){
					//PLayOnClick playScript = go.GetComponent<PLayOnClick>() as PLayOnClick;
					go.ReloadEvent(currentCheckpointIndex);
				}
			}

			
		//}
	}
	
	/* ANGLE */
	public bool isAngleReached(Vector3 angle_){
        float angle = Vector3.Angle(this.currentCheckpoint.transform.rotation.eulerAngles, angle_);
		if (angle < reachAngle) return true;
		return false;
	}
	
	/* EXT GYRO ACTIVATION */
	public void activateGyro(bool bool_){
		useGyroscope = bool_;
	}
    
	void SendTouchXY (EventBase e)
	{
        float x = e.GetParamValue("x", 0f);
        float y = e.GetParamValue("y", 0f);
		Vector3 pos = new Vector3 (x, (Screen.height - y), 0.0f);
		CamIdleTimer = 0;
		
		
        //targetLookAt = new Vector3 ( (x-Screen.width/2)*1.5f, (Screen.height/2 - y)*1.5f,objectPoolLookAtItem.transform.localPosition.z);

		Debug.Log("-- SendTouchXY() ------------------- ");
		Debug.Log("e.EventName = "+e.EventName);
		Debug.Log("x = "+x);
		Debug.Log("y = "+y);
		
		//Debug.Log("objectPoolLookAtItem x = "+objectPoolLookAtItem.transform.localPosition.x);
        //Debug.Log("objectPoolLookAtItem y = "+objectPoolLookAtItem.transform.localPosition.x);
				
		// Send a message to other element
		//if(Input.GetMouseButtonDown(0)){
			Ray rayOrigin = Camera.main.ScreenPointToRay(pos);
			RaycastHit hitInfo;
			float distance = 100;

            
            Debug.Log(string.Concat(e.EventName.ToLower(), " = ", eventDoubleTouchName.ToLower()," ", x," ", y));
            if (Physics.Raycast(rayOrigin, out hitInfo, distance))
            {
                GameObject go = hitInfo.collider.gameObject;

                if (hitInfo.collider.gameObject != null)
                {
                    if (go.GetComponent(typeof(TouchBaseClass)) && e.EventName == eventTouchStartName)
                    {
                        TouchBaseClass scriptTouch = (TouchBaseClass)go.GetComponent(typeof(TouchBaseClass));
                        scriptTouch.TriggerEvent();
                        //scriptTouch.onRayCast();
                    }
                    else if (go.GetComponent(typeof(TouchBaseClass)) && e.EventName == eventDoubleTouchName)
                    {
                        TouchBaseClass scriptTouch2 = (TouchBaseClass)go.GetComponent(typeof(TouchBaseClass));
                        //scriptTouch2 = (TouchBaseClass) go.GetComponent(typeof(TouchBaseClass));
                        scriptTouch2.MouseDoubleEvent();
                    }
                }

            }
	}
}
