using UnityEngine;
using System.Collections;

public class Noise : MonoBehaviour {
	
	private Vector3[] startPositions; 
	private Vector3[] targetPositions;
	private Mesh mesh;
	float index;
	
	// Use this for initialization
	void Start () {
		mesh = GetComponent<MeshFilter>().mesh;
        startPositions = mesh.vertices;
		targetPositions = mesh.vertices;
	 	//InvokeRepeating("RandomTarget", 0f, 5f);
	}
	
	// Update is called once per frame
	void Update () {
		index += Time.deltaTime;	
		Vector3[] vertices = mesh.vertices;
        int i = 0;
        while (i < startPositions.Length) {
			float y = Mathf.Cos(index+vertices[i].x);
			y += Mathf.Sin(index+(vertices[i].z*0.5f));
			targetPositions[i].y = y;
            vertices[i] = Vector3.Lerp(vertices[i], targetPositions[i], Time.time / 1000f);
            i++;
        }
        mesh.vertices = vertices;
        mesh.RecalculateBounds();	
	}	
}
