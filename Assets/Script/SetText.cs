using UnityEngine;
using System.Collections;

public class SetText : MonoBehaviour {
	public string stringToEdit = "Template";
	public int MaxCharacter = 20;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update(){
		GetComponent<TextMesh>().text = stringToEdit;	
	}
	
	/*
	void OnGUI()
	{
		stringToEdit = GUI.TextField(new Rect(0, 40, 150, 40), stringToEdit, MaxCharacter);
	}*/
	
	public void prefInterface(string pText){
		stringToEdit = pText;
		Debug.Log("prefInterface() = "+stringToEdit);
	}
}
