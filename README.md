#Unity BOILERPLATE#

## Description : ##
this project is used by Sien company to build and produce LiveWallpaper

## Team ##
- ALEXANDRE
- BENJAMIN

## External Unity plugins List ##
- Uni2LWP : unity to livewallpaper [Details](http://forum.unity3d.com/threads/uni2lwp-create-live-wallpapers-with-unity-3-4-2-3-5-7-4-0.172275/).
- JSONFX : json serialisation and deserialisation Library [Details](https://github.com/jsonfx/jsonfx).
- UnityVS : unity plugins for developpe on unity with visual studio. [Details](http://unityvs.com/).


## Workflow to produce liveWallpaper ##
 
1. Start developping your wallpaper on unity
2. Build `Loader` scene and `wallpaperScene` prod or dev what ever, depend of if your are testing or not.
3. Save the `Apk` file inside `Build/` folder.
4. inside Unity open menu and click :  `Window > Uni2LWP > Create live wallpaper`
5. Go inside `AndroidLWP > UnityTemplate` folder then `assets` containe your bin for live wallpaper so just copy past it to your java project a the good location 


## File organisation ##


```shell
.
├── README.md
├── AndroidLWP/ : // containing live wallpaper binary;
├── Build/ : // containing your apk builds;
├── .gitignore // ignore somes specifics files to be versionned;
└── Asset/ : Build app ready to run;
    ├── Editor/ : // contain every customs editors;
    ├── Fonts/ : // contain every fonts;
    ├── Loader/ : // contain every resources relative to first loader scene;
    ├── Materials/ : // contain every materials use by project;
    ├── Prefabs/ : // contain every games prefabs class by folders utility;
    ├── Scenes/ : // every scenes class by dev or prod folder;
        ├── DevelopmentScene/ : // development scenes container. For each Developpers duplicate the prod scene and work inside Your own duplicate scene;
        └── ProductionScene/ : // WORKING SCENE only benjamin will make updates from preprodScene to this one;
    ├── Scripts/ : // contain every scripts , class by folder;
        ├── ProjectScript/ : // contain scripts use the current specific project;
        ├── tools/ : // contain scripts use for game management;
            ├── animations/ : // contain folder specifics and re-usable animations scripts;
            ├── javaBridge/ : // contain folder specifics and re-usable javaBridge scripts ;
            └── library/ : // contain folder specifics external libs dll files;
    ├── Shaders/ : // contain every materials use by project;
    ├── Uni2LWP/ : // contain Unity to live wallpaper plugin resources;
    └── UnityVS/ : // contain Unity visual Studio plugin resources;

```
### Git use ###
- ALWAYS create branch from master when you start to work on something
- When you make a pull request : `Crée une Demande d'ajout` stop working on this branch and wait, it will be mergin fast on prod. when it will be done replace scene inside your dev directory (where your dev scene is) by prod scene.

## How to work With other Team Mates and Versionning ##

1. Copie this project where you want and set up your `GIT REPOSITORY`.

2. Create a new Branch for feature you want to work on like that: `for ben it could be BW_#numberOfbranchYouCreated_player_gamepad_control`.

3. Open your repository from unity inside a new project

4. Create your own scene if it does'nt exist by copy prod scene to you /Scenes/DevelopmentScene.

5. Work inside your own scene.

6. If you want prefabs just create your own and put it inside Prefabs if prefab you want already exist just update it.

7. When you finish to work on a specific feature make a pull request on git server or ask for your lead to merge inside PREProd Branch after test your work.
    IF YOU ARE CURRENTLY WORKING ALONE ON A PROJECT BE YOUR OWN MASTER !

8. when mergin on `PREPROD` branch just translate prefabs from your dev scene to the prod scene.

9. then merge Preprod branch inside master.

10. and Do it AGAIN and AGAIN for the rest of your project developement.

##You could Work on one single branch if you are lazy but that it's not a good Git Practice.##